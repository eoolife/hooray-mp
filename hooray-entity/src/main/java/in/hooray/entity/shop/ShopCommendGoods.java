package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_commend_goods")
public class ShopCommendGoods implements Serializable {
    @Id
    private Integer id;

    /**
     * 推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品
     */
    @Column(name = "commend_id")
    private Integer commendId;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品
     *
     * @return commend_id - 推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品
     */
    public Integer getCommendId() {
        return commendId;
    }

    /**
     * 设置推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品
     *
     * @param commendId 推荐类型ID 1:最新商品 2:特价商品 3:热卖排行 4:推荐商品
     */
    public void setCommendId(Integer commendId) {
        this.commendId = commendId;
    }

    /**
     * 获取商品ID
     *
     * @return goods_id - 商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品ID
     *
     * @param goodsId 商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", commendId=").append(commendId);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}