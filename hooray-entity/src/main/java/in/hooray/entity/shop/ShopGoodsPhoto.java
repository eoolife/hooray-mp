package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_goods_photo")
public class ShopGoodsPhoto implements Serializable {
    /**
     * 图片的md5值
     */
    @Id
    private String id;

    /**
     * 原始图片路径
     */
    private String img;

    private static final long serialVersionUID = 1L;

    /**
     * 获取图片的md5值
     *
     * @return id - 图片的md5值
     */
    public String getId() {
        return id;
    }

    /**
     * 设置图片的md5值
     *
     * @param id 图片的md5值
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 获取原始图片路径
     *
     * @return img - 原始图片路径
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置原始图片路径
     *
     * @param img 原始图片路径
     */
    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", img=").append(img);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}