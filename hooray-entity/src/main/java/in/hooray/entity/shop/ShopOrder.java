package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_order")
public class ShopOrder implements Serializable {
    @Id
    private Integer id;

    /**
     * 订单号
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 用户支付方式ID,当为0时表示货到付款
     */
    @Column(name = "pay_type")
    private Integer payType;

    /**
     * 用户选择的配送ID
     */
    private Integer distribution;

    /**
     * 订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款
     */
    private Boolean status;

    /**
     * 支付状态 0：未支付; 1：已支付;
     */
    @Column(name = "pay_status")
    private Boolean payStatus;

    /**
     * 配送状态 0：未发送,1：已发送,2：部分发送
     */
    @Column(name = "distribution_status")
    private Boolean distributionStatus;

    /**
     * 收货人姓名
     */
    @Column(name = "accept_name")
    private String acceptName;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 联系电话
     */
    private String telphone;

    /**
     * 国ID
     */
    private Integer country;

    /**
     * 省ID
     */
    private Integer province;

    /**
     * 市ID
     */
    private Integer city;

    /**
     * 区ID
     */
    private Integer area;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 应付商品总金额
     */
    @Column(name = "payable_amount")
    private BigDecimal payableAmount;

    /**
     * 实付商品总金额
     */
    @Column(name = "real_amount")
    private BigDecimal realAmount;

    /**
     * 总运费金额
     */
    @Column(name = "payable_freight")
    private BigDecimal payableFreight;

    /**
     * 实付运费
     */
    @Column(name = "real_freight")
    private BigDecimal realFreight;

    /**
     * 付款时间
     */
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 发货时间
     */
    @Column(name = "send_time")
    private Date sendTime;

    /**
     * 下单时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 订单完成时间
     */
    @Column(name = "completion_time")
    private Date completionTime;

    /**
     * 发票：0不索要1索要
     */
    private Boolean invoice;

    /**
     * 用户附言
     */
    private String postscript;

    /**
     * 是否删除1为删除
     */
    @Column(name = "if_del")
    private Boolean ifDel;

    /**
     * 保价
     */
    private BigDecimal insured;

    /**
     * 是否保价0:不保价，1保价
     */
    @Column(name = "if_insured")
    private Boolean ifInsured;

    /**
     * 支付手续费
     */
    @Column(name = "pay_fee")
    private BigDecimal payFee;

    /**
     * 发票抬头
     */
    @Column(name = "invoice_title")
    private String invoiceTitle;

    /**
     * 税金
     */
    private BigDecimal taxes;

    /**
     * 促销优惠金额
     */
    private BigDecimal promotions;

    /**
     * 订单折扣或涨价
     */
    private BigDecimal discount;

    /**
     * 订单总金额
     */
    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 使用的道具id
     */
    private String prop;

    /**
     * 用户收货时间
     */
    @Column(name = "accept_time")
    private String acceptTime;

    /**
     * 增加的经验
     */
    private Short exp;

    /**
     * 增加的积分
     */
    private Short point;

    /**
     * 0普通订单,1团购订单,2限时抢购
     */
    private Boolean type;

    /**
     * 支付平台交易号
     */
    @Column(name = "trade_no")
    private String tradeNo;

    /**
     * 自提点ID
     */
    private Integer takeself;

    /**
     * 自提方式的验证码
     */
    private String checkcode;

    /**
     * 促销活动ID
     */
    @Column(name = "active_id")
    private Integer activeId;

    /**
     * 管理员备注
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单号
     *
     * @return order_no - 订单号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 设置订单号
     *
     * @param orderNo 订单号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取用户支付方式ID,当为0时表示货到付款
     *
     * @return pay_type - 用户支付方式ID,当为0时表示货到付款
     */
    public Integer getPayType() {
        return payType;
    }

    /**
     * 设置用户支付方式ID,当为0时表示货到付款
     *
     * @param payType 用户支付方式ID,当为0时表示货到付款
     */
    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    /**
     * 获取用户选择的配送ID
     *
     * @return distribution - 用户选择的配送ID
     */
    public Integer getDistribution() {
        return distribution;
    }

    /**
     * 设置用户选择的配送ID
     *
     * @param distribution 用户选择的配送ID
     */
    public void setDistribution(Integer distribution) {
        this.distribution = distribution;
    }

    /**
     * 获取订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款
     *
     * @return status - 订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款
     *
     * @param status 订单状态 1生成订单,2支付订单,3取消订单(客户触发),4作废订单(管理员触发),5完成订单,6退款,7部分退款
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取支付状态 0：未支付; 1：已支付;
     *
     * @return pay_status - 支付状态 0：未支付; 1：已支付;
     */
    public Boolean getPayStatus() {
        return payStatus;
    }

    /**
     * 设置支付状态 0：未支付; 1：已支付;
     *
     * @param payStatus 支付状态 0：未支付; 1：已支付;
     */
    public void setPayStatus(Boolean payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 获取配送状态 0：未发送,1：已发送,2：部分发送
     *
     * @return distribution_status - 配送状态 0：未发送,1：已发送,2：部分发送
     */
    public Boolean getDistributionStatus() {
        return distributionStatus;
    }

    /**
     * 设置配送状态 0：未发送,1：已发送,2：部分发送
     *
     * @param distributionStatus 配送状态 0：未发送,1：已发送,2：部分发送
     */
    public void setDistributionStatus(Boolean distributionStatus) {
        this.distributionStatus = distributionStatus;
    }

    /**
     * 获取收货人姓名
     *
     * @return accept_name - 收货人姓名
     */
    public String getAcceptName() {
        return acceptName;
    }

    /**
     * 设置收货人姓名
     *
     * @param acceptName 收货人姓名
     */
    public void setAcceptName(String acceptName) {
        this.acceptName = acceptName == null ? null : acceptName.trim();
    }

    /**
     * 获取邮编
     *
     * @return postcode - 邮编
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 设置邮编
     *
     * @param postcode 邮编
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    /**
     * 获取联系电话
     *
     * @return telphone - 联系电话
     */
    public String getTelphone() {
        return telphone;
    }

    /**
     * 设置联系电话
     *
     * @param telphone 联系电话
     */
    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    /**
     * 获取国ID
     *
     * @return country - 国ID
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 设置国ID
     *
     * @param country 国ID
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 获取省ID
     *
     * @return province - 省ID
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 设置省ID
     *
     * @param province 省ID
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 获取市ID
     *
     * @return city - 市ID
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 设置市ID
     *
     * @param city 市ID
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 获取区ID
     *
     * @return area - 区ID
     */
    public Integer getArea() {
        return area;
    }

    /**
     * 设置区ID
     *
     * @param area 区ID
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取收货地址
     *
     * @return address - 收货地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置收货地址
     *
     * @param address 收货地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取手机
     *
     * @return mobile - 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机
     *
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取应付商品总金额
     *
     * @return payable_amount - 应付商品总金额
     */
    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    /**
     * 设置应付商品总金额
     *
     * @param payableAmount 应付商品总金额
     */
    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    /**
     * 获取实付商品总金额
     *
     * @return real_amount - 实付商品总金额
     */
    public BigDecimal getRealAmount() {
        return realAmount;
    }

    /**
     * 设置实付商品总金额
     *
     * @param realAmount 实付商品总金额
     */
    public void setRealAmount(BigDecimal realAmount) {
        this.realAmount = realAmount;
    }

    /**
     * 获取总运费金额
     *
     * @return payable_freight - 总运费金额
     */
    public BigDecimal getPayableFreight() {
        return payableFreight;
    }

    /**
     * 设置总运费金额
     *
     * @param payableFreight 总运费金额
     */
    public void setPayableFreight(BigDecimal payableFreight) {
        this.payableFreight = payableFreight;
    }

    /**
     * 获取实付运费
     *
     * @return real_freight - 实付运费
     */
    public BigDecimal getRealFreight() {
        return realFreight;
    }

    /**
     * 设置实付运费
     *
     * @param realFreight 实付运费
     */
    public void setRealFreight(BigDecimal realFreight) {
        this.realFreight = realFreight;
    }

    /**
     * 获取付款时间
     *
     * @return pay_time - 付款时间
     */
    public Date getPayTime() {
        return payTime;
    }

    /**
     * 设置付款时间
     *
     * @param payTime 付款时间
     */
    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    /**
     * 获取发货时间
     *
     * @return send_time - 发货时间
     */
    public Date getSendTime() {
        return sendTime;
    }

    /**
     * 设置发货时间
     *
     * @param sendTime 发货时间
     */
    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    /**
     * 获取下单时间
     *
     * @return create_time - 下单时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置下单时间
     *
     * @param createTime 下单时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取订单完成时间
     *
     * @return completion_time - 订单完成时间
     */
    public Date getCompletionTime() {
        return completionTime;
    }

    /**
     * 设置订单完成时间
     *
     * @param completionTime 订单完成时间
     */
    public void setCompletionTime(Date completionTime) {
        this.completionTime = completionTime;
    }

    /**
     * 获取发票：0不索要1索要
     *
     * @return invoice - 发票：0不索要1索要
     */
    public Boolean getInvoice() {
        return invoice;
    }

    /**
     * 设置发票：0不索要1索要
     *
     * @param invoice 发票：0不索要1索要
     */
    public void setInvoice(Boolean invoice) {
        this.invoice = invoice;
    }

    /**
     * 获取用户附言
     *
     * @return postscript - 用户附言
     */
    public String getPostscript() {
        return postscript;
    }

    /**
     * 设置用户附言
     *
     * @param postscript 用户附言
     */
    public void setPostscript(String postscript) {
        this.postscript = postscript == null ? null : postscript.trim();
    }

    /**
     * 获取是否删除1为删除
     *
     * @return if_del - 是否删除1为删除
     */
    public Boolean getIfDel() {
        return ifDel;
    }

    /**
     * 设置是否删除1为删除
     *
     * @param ifDel 是否删除1为删除
     */
    public void setIfDel(Boolean ifDel) {
        this.ifDel = ifDel;
    }

    /**
     * 获取保价
     *
     * @return insured - 保价
     */
    public BigDecimal getInsured() {
        return insured;
    }

    /**
     * 设置保价
     *
     * @param insured 保价
     */
    public void setInsured(BigDecimal insured) {
        this.insured = insured;
    }

    /**
     * 获取是否保价0:不保价，1保价
     *
     * @return if_insured - 是否保价0:不保价，1保价
     */
    public Boolean getIfInsured() {
        return ifInsured;
    }

    /**
     * 设置是否保价0:不保价，1保价
     *
     * @param ifInsured 是否保价0:不保价，1保价
     */
    public void setIfInsured(Boolean ifInsured) {
        this.ifInsured = ifInsured;
    }

    /**
     * 获取支付手续费
     *
     * @return pay_fee - 支付手续费
     */
    public BigDecimal getPayFee() {
        return payFee;
    }

    /**
     * 设置支付手续费
     *
     * @param payFee 支付手续费
     */
    public void setPayFee(BigDecimal payFee) {
        this.payFee = payFee;
    }

    /**
     * 获取发票抬头
     *
     * @return invoice_title - 发票抬头
     */
    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    /**
     * 设置发票抬头
     *
     * @param invoiceTitle 发票抬头
     */
    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle == null ? null : invoiceTitle.trim();
    }

    /**
     * 获取税金
     *
     * @return taxes - 税金
     */
    public BigDecimal getTaxes() {
        return taxes;
    }

    /**
     * 设置税金
     *
     * @param taxes 税金
     */
    public void setTaxes(BigDecimal taxes) {
        this.taxes = taxes;
    }

    /**
     * 获取促销优惠金额
     *
     * @return promotions - 促销优惠金额
     */
    public BigDecimal getPromotions() {
        return promotions;
    }

    /**
     * 设置促销优惠金额
     *
     * @param promotions 促销优惠金额
     */
    public void setPromotions(BigDecimal promotions) {
        this.promotions = promotions;
    }

    /**
     * 获取订单折扣或涨价
     *
     * @return discount - 订单折扣或涨价
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * 设置订单折扣或涨价
     *
     * @param discount 订单折扣或涨价
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * 获取订单总金额
     *
     * @return order_amount - 订单总金额
     */
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    /**
     * 设置订单总金额
     *
     * @param orderAmount 订单总金额
     */
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * 获取使用的道具id
     *
     * @return prop - 使用的道具id
     */
    public String getProp() {
        return prop;
    }

    /**
     * 设置使用的道具id
     *
     * @param prop 使用的道具id
     */
    public void setProp(String prop) {
        this.prop = prop == null ? null : prop.trim();
    }

    /**
     * 获取用户收货时间
     *
     * @return accept_time - 用户收货时间
     */
    public String getAcceptTime() {
        return acceptTime;
    }

    /**
     * 设置用户收货时间
     *
     * @param acceptTime 用户收货时间
     */
    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime == null ? null : acceptTime.trim();
    }

    /**
     * 获取增加的经验
     *
     * @return exp - 增加的经验
     */
    public Short getExp() {
        return exp;
    }

    /**
     * 设置增加的经验
     *
     * @param exp 增加的经验
     */
    public void setExp(Short exp) {
        this.exp = exp;
    }

    /**
     * 获取增加的积分
     *
     * @return point - 增加的积分
     */
    public Short getPoint() {
        return point;
    }

    /**
     * 设置增加的积分
     *
     * @param point 增加的积分
     */
    public void setPoint(Short point) {
        this.point = point;
    }

    /**
     * 获取0普通订单,1团购订单,2限时抢购
     *
     * @return type - 0普通订单,1团购订单,2限时抢购
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置0普通订单,1团购订单,2限时抢购
     *
     * @param type 0普通订单,1团购订单,2限时抢购
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取支付平台交易号
     *
     * @return trade_no - 支付平台交易号
     */
    public String getTradeNo() {
        return tradeNo;
    }

    /**
     * 设置支付平台交易号
     *
     * @param tradeNo 支付平台交易号
     */
    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo == null ? null : tradeNo.trim();
    }

    /**
     * 获取自提点ID
     *
     * @return takeself - 自提点ID
     */
    public Integer getTakeself() {
        return takeself;
    }

    /**
     * 设置自提点ID
     *
     * @param takeself 自提点ID
     */
    public void setTakeself(Integer takeself) {
        this.takeself = takeself;
    }

    /**
     * 获取自提方式的验证码
     *
     * @return checkcode - 自提方式的验证码
     */
    public String getCheckcode() {
        return checkcode;
    }

    /**
     * 设置自提方式的验证码
     *
     * @param checkcode 自提方式的验证码
     */
    public void setCheckcode(String checkcode) {
        this.checkcode = checkcode == null ? null : checkcode.trim();
    }

    /**
     * 获取促销活动ID
     *
     * @return active_id - 促销活动ID
     */
    public Integer getActiveId() {
        return activeId;
    }

    /**
     * 设置促销活动ID
     *
     * @param activeId 促销活动ID
     */
    public void setActiveId(Integer activeId) {
        this.activeId = activeId;
    }

    /**
     * 获取管理员备注
     *
     * @return note - 管理员备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置管理员备注
     *
     * @param note 管理员备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", userId=").append(userId);
        sb.append(", payType=").append(payType);
        sb.append(", distribution=").append(distribution);
        sb.append(", status=").append(status);
        sb.append(", payStatus=").append(payStatus);
        sb.append(", distributionStatus=").append(distributionStatus);
        sb.append(", acceptName=").append(acceptName);
        sb.append(", postcode=").append(postcode);
        sb.append(", telphone=").append(telphone);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", address=").append(address);
        sb.append(", mobile=").append(mobile);
        sb.append(", payableAmount=").append(payableAmount);
        sb.append(", realAmount=").append(realAmount);
        sb.append(", payableFreight=").append(payableFreight);
        sb.append(", realFreight=").append(realFreight);
        sb.append(", payTime=").append(payTime);
        sb.append(", sendTime=").append(sendTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", completionTime=").append(completionTime);
        sb.append(", invoice=").append(invoice);
        sb.append(", postscript=").append(postscript);
        sb.append(", ifDel=").append(ifDel);
        sb.append(", insured=").append(insured);
        sb.append(", ifInsured=").append(ifInsured);
        sb.append(", payFee=").append(payFee);
        sb.append(", invoiceTitle=").append(invoiceTitle);
        sb.append(", taxes=").append(taxes);
        sb.append(", promotions=").append(promotions);
        sb.append(", discount=").append(discount);
        sb.append(", orderAmount=").append(orderAmount);
        sb.append(", prop=").append(prop);
        sb.append(", acceptTime=").append(acceptTime);
        sb.append(", exp=").append(exp);
        sb.append(", point=").append(point);
        sb.append(", type=").append(type);
        sb.append(", tradeNo=").append(tradeNo);
        sb.append(", takeself=").append(takeself);
        sb.append(", checkcode=").append(checkcode);
        sb.append(", activeId=").append(activeId);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}