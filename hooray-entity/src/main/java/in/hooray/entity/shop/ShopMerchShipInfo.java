package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_merch_ship_info")
public class ShopMerchShipInfo implements Serializable {
    @Id
    private Integer id;

    /**
     * 发货点名称
     */
    @Column(name = "ship_name")
    private String shipName;

    /**
     * 发货人姓名
     */
    @Column(name = "ship_user_name")
    private String shipUserName;

    /**
     * 性别 0:女 1:男
     */
    private Boolean sex;

    /**
     * 国id
     */
    private Integer country;

    /**
     * 省id
     */
    private Integer province;

    /**
     * 市id
     */
    private Integer city;

    /**
     * 地区id
     */
    private Integer area;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 具体地址
     */
    private String address;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 电话
     */
    private String telphone;

    /**
     * 1为默认地址，0则不是
     */
    @Column(name = "is_default")
    private Boolean isDefault;

    /**
     * 保存时间
     */
    private Date addtime;

    /**
     * 0为删除，1为显示
     */
    @Column(name = "is_del")
    private Boolean isDel;

    /**
     * 备注
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取发货点名称
     *
     * @return ship_name - 发货点名称
     */
    public String getShipName() {
        return shipName;
    }

    /**
     * 设置发货点名称
     *
     * @param shipName 发货点名称
     */
    public void setShipName(String shipName) {
        this.shipName = shipName == null ? null : shipName.trim();
    }

    /**
     * 获取发货人姓名
     *
     * @return ship_user_name - 发货人姓名
     */
    public String getShipUserName() {
        return shipUserName;
    }

    /**
     * 设置发货人姓名
     *
     * @param shipUserName 发货人姓名
     */
    public void setShipUserName(String shipUserName) {
        this.shipUserName = shipUserName == null ? null : shipUserName.trim();
    }

    /**
     * 获取性别 0:女 1:男
     *
     * @return sex - 性别 0:女 1:男
     */
    public Boolean getSex() {
        return sex;
    }

    /**
     * 设置性别 0:女 1:男
     *
     * @param sex 性别 0:女 1:男
     */
    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    /**
     * 获取国id
     *
     * @return country - 国id
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 设置国id
     *
     * @param country 国id
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 获取省id
     *
     * @return province - 省id
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 设置省id
     *
     * @param province 省id
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 获取市id
     *
     * @return city - 市id
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 设置市id
     *
     * @param city 市id
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 获取地区id
     *
     * @return area - 地区id
     */
    public Integer getArea() {
        return area;
    }

    /**
     * 设置地区id
     *
     * @param area 地区id
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取邮编
     *
     * @return postcode - 邮编
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 设置邮编
     *
     * @param postcode 邮编
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    /**
     * 获取具体地址
     *
     * @return address - 具体地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置具体地址
     *
     * @param address 具体地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取手机
     *
     * @return mobile - 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机
     *
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取电话
     *
     * @return telphone - 电话
     */
    public String getTelphone() {
        return telphone;
    }

    /**
     * 设置电话
     *
     * @param telphone 电话
     */
    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    /**
     * 获取1为默认地址，0则不是
     *
     * @return is_default - 1为默认地址，0则不是
     */
    public Boolean getIsDefault() {
        return isDefault;
    }

    /**
     * 设置1为默认地址，0则不是
     *
     * @param isDefault 1为默认地址，0则不是
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * 获取保存时间
     *
     * @return addtime - 保存时间
     */
    public Date getAddtime() {
        return addtime;
    }

    /**
     * 设置保存时间
     *
     * @param addtime 保存时间
     */
    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    /**
     * 获取0为删除，1为显示
     *
     * @return is_del - 0为删除，1为显示
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置0为删除，1为显示
     *
     * @param isDel 0为删除，1为显示
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", shipName=").append(shipName);
        sb.append(", shipUserName=").append(shipUserName);
        sb.append(", sex=").append(sex);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", postcode=").append(postcode);
        sb.append(", address=").append(address);
        sb.append(", mobile=").append(mobile);
        sb.append(", telphone=").append(telphone);
        sb.append(", isDefault=").append(isDefault);
        sb.append(", addtime=").append(addtime);
        sb.append(", isDel=").append(isDel);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}