package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "shop_products")
public class ShopProducts implements Serializable {
    @Id
    private Integer id;

    /**
     * 货品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 货品的货号(以商品的货号加横线加数字组成)
     */
    @Column(name = "products_no")
    private String productsNo;

    /**
     * 库存
     */
    @Column(name = "store_nums")
    private Integer storeNums;

    /**
     * 市场价格
     */
    @Column(name = "market_price")
    private BigDecimal marketPrice;

    /**
     * 销售价格
     */
    @Column(name = "sell_price")
    private BigDecimal sellPrice;

    /**
     * 成本价格
     */
    @Column(name = "cost_price")
    private BigDecimal costPrice;

    /**
     * 重量
     */
    private BigDecimal weight;

    /**
     * json规格数据
     */
    @Column(name = "spec_array")
    private String specArray;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取货品ID
     *
     * @return goods_id - 货品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置货品ID
     *
     * @param goodsId 货品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取货品的货号(以商品的货号加横线加数字组成)
     *
     * @return products_no - 货品的货号(以商品的货号加横线加数字组成)
     */
    public String getProductsNo() {
        return productsNo;
    }

    /**
     * 设置货品的货号(以商品的货号加横线加数字组成)
     *
     * @param productsNo 货品的货号(以商品的货号加横线加数字组成)
     */
    public void setProductsNo(String productsNo) {
        this.productsNo = productsNo == null ? null : productsNo.trim();
    }

    /**
     * 获取库存
     *
     * @return store_nums - 库存
     */
    public Integer getStoreNums() {
        return storeNums;
    }

    /**
     * 设置库存
     *
     * @param storeNums 库存
     */
    public void setStoreNums(Integer storeNums) {
        this.storeNums = storeNums;
    }

    /**
     * 获取市场价格
     *
     * @return market_price - 市场价格
     */
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    /**
     * 设置市场价格
     *
     * @param marketPrice 市场价格
     */
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * 获取销售价格
     *
     * @return sell_price - 销售价格
     */
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置销售价格
     *
     * @param sellPrice 销售价格
     */
    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取成本价格
     *
     * @return cost_price - 成本价格
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * 设置成本价格
     *
     * @param costPrice 成本价格
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * 获取重量
     *
     * @return weight - 重量
     */
    public BigDecimal getWeight() {
        return weight;
    }

    /**
     * 设置重量
     *
     * @param weight 重量
     */
    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    /**
     * 获取json规格数据
     *
     * @return spec_array - json规格数据
     */
    public String getSpecArray() {
        return specArray;
    }

    /**
     * 设置json规格数据
     *
     * @param specArray json规格数据
     */
    public void setSpecArray(String specArray) {
        this.specArray = specArray == null ? null : specArray.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", productsNo=").append(productsNo);
        sb.append(", storeNums=").append(storeNums);
        sb.append(", marketPrice=").append(marketPrice);
        sb.append(", sellPrice=").append(sellPrice);
        sb.append(", costPrice=").append(costPrice);
        sb.append(", weight=").append(weight);
        sb.append(", specArray=").append(specArray);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}