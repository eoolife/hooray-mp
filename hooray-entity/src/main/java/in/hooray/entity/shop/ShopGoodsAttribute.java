package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_goods_attribute")
public class ShopGoodsAttribute implements Serializable {
    @Id
    private Integer id;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Integer goodsId;

    /**
     * 属性ID
     */
    @Column(name = "attribute_id")
    private Integer attributeId;

    /**
     * 属性值
     */
    @Column(name = "attribute_value")
    private String attributeValue;

    /**
     * 模型ID
     */
    @Column(name = "model_id")
    private Integer modelId;

    /**
     * 排序
     */
    private Short order;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品ID
     *
     * @return goods_id - 商品ID
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品ID
     *
     * @param goodsId 商品ID
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 获取属性ID
     *
     * @return attribute_id - 属性ID
     */
    public Integer getAttributeId() {
        return attributeId;
    }

    /**
     * 设置属性ID
     *
     * @param attributeId 属性ID
     */
    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    /**
     * 获取属性值
     *
     * @return attribute_value - 属性值
     */
    public String getAttributeValue() {
        return attributeValue;
    }

    /**
     * 设置属性值
     *
     * @param attributeValue 属性值
     */
    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue == null ? null : attributeValue.trim();
    }

    /**
     * 获取模型ID
     *
     * @return model_id - 模型ID
     */
    public Integer getModelId() {
        return modelId;
    }

    /**
     * 设置模型ID
     *
     * @param modelId 模型ID
     */
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    /**
     * 获取排序
     *
     * @return order - 排序
     */
    public Short getOrder() {
        return order;
    }

    /**
     * 设置排序
     *
     * @param order 排序
     */
    public void setOrder(Short order) {
        this.order = order;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", attributeId=").append(attributeId);
        sb.append(", attributeValue=").append(attributeValue);
        sb.append(", modelId=").append(modelId);
        sb.append(", order=").append(order);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}