package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_freight_company")
public class ShopFreightCompany implements Serializable {
    @Id
    private Integer id;

    /**
     * 货运类型
     */
    @Column(name = "freight_type")
    private String freightType;

    /**
     * 货运公司名称
     */
    @Column(name = "freight_name")
    private String freightName;

    /**
     * 网址
     */
    private String url;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 0未删除1删除
     */
    @Column(name = "is_del")
    private Boolean isDel;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取货运类型
     *
     * @return freight_type - 货运类型
     */
    public String getFreightType() {
        return freightType;
    }

    /**
     * 设置货运类型
     *
     * @param freightType 货运类型
     */
    public void setFreightType(String freightType) {
        this.freightType = freightType == null ? null : freightType.trim();
    }

    /**
     * 获取货运公司名称
     *
     * @return freight_name - 货运公司名称
     */
    public String getFreightName() {
        return freightName;
    }

    /**
     * 设置货运公司名称
     *
     * @param freightName 货运公司名称
     */
    public void setFreightName(String freightName) {
        this.freightName = freightName == null ? null : freightName.trim();
    }

    /**
     * 获取网址
     *
     * @return url - 网址
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置网址
     *
     * @param url 网址
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取0未删除1删除
     *
     * @return is_del - 0未删除1删除
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置0未删除1删除
     *
     * @param isDel 0未删除1删除
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", freightType=").append(freightType);
        sb.append(", freightName=").append(freightName);
        sb.append(", url=").append(url);
        sb.append(", sort=").append(sort);
        sb.append(", isDel=").append(isDel);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}