package in.hooray.entity.shop;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_point_log")
public class ShopPointLog implements Serializable {
    @Id
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 发生时间
     */
    private Date datetime;

    /**
     * 积分增减 增加正数 减少负数
     */
    private Integer value;

    /**
     * 积分改动说明
     */
    private String intro;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取发生时间
     *
     * @return datetime - 发生时间
     */
    public Date getDatetime() {
        return datetime;
    }

    /**
     * 设置发生时间
     *
     * @param datetime 发生时间
     */
    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    /**
     * 获取积分增减 增加正数 减少负数
     *
     * @return value - 积分增减 增加正数 减少负数
     */
    public Integer getValue() {
        return value;
    }

    /**
     * 设置积分增减 增加正数 减少负数
     *
     * @param value 积分增减 增加正数 减少负数
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * 获取积分改动说明
     *
     * @return intro - 积分改动说明
     */
    public String getIntro() {
        return intro;
    }

    /**
     * 设置积分改动说明
     *
     * @param intro 积分改动说明
     */
    public void setIntro(String intro) {
        this.intro = intro == null ? null : intro.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", datetime=").append(datetime);
        sb.append(", value=").append(value);
        sb.append(", intro=").append(intro);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}