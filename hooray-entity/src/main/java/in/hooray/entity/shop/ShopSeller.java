package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_seller")
public class ShopSeller implements Serializable {
    @Id
    private Integer id;

    /**
     * 商家登录用户名
     */
    @Column(name = "seller_name")
    private String sellerName;

    /**
     * 商家密码
     */
    private String password;

    /**
     * 加入时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 最后登录时间
     */
    @Column(name = "login_time")
    private Date loginTime;

    /**
     * 是否是特级商家
     */
    @Column(name = "is_vip")
    private Boolean isVip;

    /**
     * 0:未删除,1:已删除
     */
    @Column(name = "is_del")
    private Boolean isDel;

    /**
     * 0:未锁定,1:已锁定
     */
    @Column(name = "is_lock")
    private Boolean isLock;

    /**
     * 商家真实名称
     */
    @Column(name = "true_name")
    private String trueName;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 座机号码
     */
    private String phone;

    /**
     * 执照证件照片
     */
    @Column(name = "paper_img")
    private String paperImg;

    /**
     * 保证金
     */
    private BigDecimal cash;

    /**
     * 国ID
     */
    private Integer country;

    /**
     * 省ID
     */
    private Integer province;

    /**
     * 市ID
     */
    private Integer city;

    /**
     * 区ID
     */
    private Integer area;

    /**
     * 地址
     */
    private String address;

    /**
     * 客服号码
     */
    @Column(name = "server_num")
    private String serverNum;

    /**
     * 企业URL网站
     */
    @Column(name = "home_url")
    private String homeUrl;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 收款账号信息
     */
    private String account;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商家登录用户名
     *
     * @return seller_name - 商家登录用户名
     */
    public String getSellerName() {
        return sellerName;
    }

    /**
     * 设置商家登录用户名
     *
     * @param sellerName 商家登录用户名
     */
    public void setSellerName(String sellerName) {
        this.sellerName = sellerName == null ? null : sellerName.trim();
    }

    /**
     * 获取商家密码
     *
     * @return password - 商家密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置商家密码
     *
     * @param password 商家密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取加入时间
     *
     * @return create_time - 加入时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置加入时间
     *
     * @param createTime 加入时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取最后登录时间
     *
     * @return login_time - 最后登录时间
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * 设置最后登录时间
     *
     * @param loginTime 最后登录时间
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * 获取是否是特级商家
     *
     * @return is_vip - 是否是特级商家
     */
    public Boolean getIsVip() {
        return isVip;
    }

    /**
     * 设置是否是特级商家
     *
     * @param isVip 是否是特级商家
     */
    public void setIsVip(Boolean isVip) {
        this.isVip = isVip;
    }

    /**
     * 获取0:未删除,1:已删除
     *
     * @return is_del - 0:未删除,1:已删除
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置0:未删除,1:已删除
     *
     * @param isDel 0:未删除,1:已删除
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    /**
     * 获取0:未锁定,1:已锁定
     *
     * @return is_lock - 0:未锁定,1:已锁定
     */
    public Boolean getIsLock() {
        return isLock;
    }

    /**
     * 设置0:未锁定,1:已锁定
     *
     * @param isLock 0:未锁定,1:已锁定
     */
    public void setIsLock(Boolean isLock) {
        this.isLock = isLock;
    }

    /**
     * 获取商家真实名称
     *
     * @return true_name - 商家真实名称
     */
    public String getTrueName() {
        return trueName;
    }

    /**
     * 设置商家真实名称
     *
     * @param trueName 商家真实名称
     */
    public void setTrueName(String trueName) {
        this.trueName = trueName == null ? null : trueName.trim();
    }

    /**
     * 获取电子邮箱
     *
     * @return email - 电子邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置电子邮箱
     *
     * @param email 电子邮箱
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * 获取手机号码
     *
     * @return mobile - 手机号码
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号码
     *
     * @param mobile 手机号码
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取座机号码
     *
     * @return phone - 座机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置座机号码
     *
     * @param phone 座机号码
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 获取执照证件照片
     *
     * @return paper_img - 执照证件照片
     */
    public String getPaperImg() {
        return paperImg;
    }

    /**
     * 设置执照证件照片
     *
     * @param paperImg 执照证件照片
     */
    public void setPaperImg(String paperImg) {
        this.paperImg = paperImg == null ? null : paperImg.trim();
    }

    /**
     * 获取保证金
     *
     * @return cash - 保证金
     */
    public BigDecimal getCash() {
        return cash;
    }

    /**
     * 设置保证金
     *
     * @param cash 保证金
     */
    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    /**
     * 获取国ID
     *
     * @return country - 国ID
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 设置国ID
     *
     * @param country 国ID
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 获取省ID
     *
     * @return province - 省ID
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 设置省ID
     *
     * @param province 省ID
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 获取市ID
     *
     * @return city - 市ID
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 设置市ID
     *
     * @param city 市ID
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 获取区ID
     *
     * @return area - 区ID
     */
    public Integer getArea() {
        return area;
    }

    /**
     * 设置区ID
     *
     * @param area 区ID
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取地址
     *
     * @return address - 地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置地址
     *
     * @param address 地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取客服号码
     *
     * @return server_num - 客服号码
     */
    public String getServerNum() {
        return serverNum;
    }

    /**
     * 设置客服号码
     *
     * @param serverNum 客服号码
     */
    public void setServerNum(String serverNum) {
        this.serverNum = serverNum == null ? null : serverNum.trim();
    }

    /**
     * 获取企业URL网站
     *
     * @return home_url - 企业URL网站
     */
    public String getHomeUrl() {
        return homeUrl;
    }

    /**
     * 设置企业URL网站
     *
     * @param homeUrl 企业URL网站
     */
    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl == null ? null : homeUrl.trim();
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取收款账号信息
     *
     * @return account - 收款账号信息
     */
    public String getAccount() {
        return account;
    }

    /**
     * 设置收款账号信息
     *
     * @param account 收款账号信息
     */
    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", sellerName=").append(sellerName);
        sb.append(", password=").append(password);
        sb.append(", createTime=").append(createTime);
        sb.append(", loginTime=").append(loginTime);
        sb.append(", isVip=").append(isVip);
        sb.append(", isDel=").append(isDel);
        sb.append(", isLock=").append(isLock);
        sb.append(", trueName=").append(trueName);
        sb.append(", email=").append(email);
        sb.append(", mobile=").append(mobile);
        sb.append(", phone=").append(phone);
        sb.append(", paperImg=").append(paperImg);
        sb.append(", cash=").append(cash);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", address=").append(address);
        sb.append(", serverNum=").append(serverNum);
        sb.append(", homeUrl=").append(homeUrl);
        sb.append(", sort=").append(sort);
        sb.append(", account=").append(account);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}