package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_expresswaybill")
public class ShopExpresswayBill implements Serializable {
    @Id
    private Integer id;

    /**
     * 快递单模板名字
     */
    private String name;

    /**
     * 背景图片路径
     */
    private String background;

    /**
     * 背景图片路径
     */
    private Short width;

    /**
     * 背景图片路径
     */
    private Short height;

    /**
     * 状态 1关闭,0正常
     */
    @Column(name = "is_close")
    private Boolean isClose;

    /**
     * 序列化的快递单结构数据
     */
    private String config;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取快递单模板名字
     *
     * @return name - 快递单模板名字
     */
    public String getName() {
        return name;
    }

    /**
     * 设置快递单模板名字
     *
     * @param name 快递单模板名字
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取背景图片路径
     *
     * @return background - 背景图片路径
     */
    public String getBackground() {
        return background;
    }

    /**
     * 设置背景图片路径
     *
     * @param background 背景图片路径
     */
    public void setBackground(String background) {
        this.background = background == null ? null : background.trim();
    }

    /**
     * 获取背景图片路径
     *
     * @return width - 背景图片路径
     */
    public Short getWidth() {
        return width;
    }

    /**
     * 设置背景图片路径
     *
     * @param width 背景图片路径
     */
    public void setWidth(Short width) {
        this.width = width;
    }

    /**
     * 获取背景图片路径
     *
     * @return height - 背景图片路径
     */
    public Short getHeight() {
        return height;
    }

    /**
     * 设置背景图片路径
     *
     * @param height 背景图片路径
     */
    public void setHeight(Short height) {
        this.height = height;
    }

    /**
     * 获取状态 1关闭,0正常
     *
     * @return is_close - 状态 1关闭,0正常
     */
    public Boolean getIsClose() {
        return isClose;
    }

    /**
     * 设置状态 1关闭,0正常
     *
     * @param isClose 状态 1关闭,0正常
     */
    public void setIsClose(Boolean isClose) {
        this.isClose = isClose;
    }

    /**
     * 获取序列化的快递单结构数据
     *
     * @return config - 序列化的快递单结构数据
     */
    public String getConfig() {
        return config;
    }

    /**
     * 设置序列化的快递单结构数据
     *
     * @param config 序列化的快递单结构数据
     */
    public void setConfig(String config) {
        this.config = config == null ? null : config.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", background=").append(background);
        sb.append(", width=").append(width);
        sb.append(", height=").append(height);
        sb.append(", isClose=").append(isClose);
        sb.append(", config=").append(config);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}