package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_withdraw")
public class ShopWithdraw implements Serializable {
    @Id
    private Integer id;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 时间
     */
    private Date time;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 开户姓名
     */
    private String name;

    /**
     * -1失败,0未处理,1处理中,2成功
     */
    private Boolean status;

    /**
     * 用户备注
     */
    private String note;

    /**
     * 回复备注信息
     */
    @Column(name = "re_note")
    private String reNote;

    /**
     * 0未删除,1已删除
     */
    @Column(name = "is_del")
    private Boolean isDel;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取开户姓名
     *
     * @return name - 开户姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置开户姓名
     *
     * @param name 开户姓名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取-1失败,0未处理,1处理中,2成功
     *
     * @return status - -1失败,0未处理,1处理中,2成功
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * 设置-1失败,0未处理,1处理中,2成功
     *
     * @param status -1失败,0未处理,1处理中,2成功
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * 获取用户备注
     *
     * @return note - 用户备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置用户备注
     *
     * @param note 用户备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * 获取回复备注信息
     *
     * @return re_note - 回复备注信息
     */
    public String getReNote() {
        return reNote;
    }

    /**
     * 设置回复备注信息
     *
     * @param reNote 回复备注信息
     */
    public void setReNote(String reNote) {
        this.reNote = reNote == null ? null : reNote.trim();
    }

    /**
     * 获取0未删除,1已删除
     *
     * @return is_del - 0未删除,1已删除
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置0未删除,1已删除
     *
     * @param isDel 0未删除,1已删除
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", time=").append(time);
        sb.append(", amount=").append(amount);
        sb.append(", name=").append(name);
        sb.append(", status=").append(status);
        sb.append(", note=").append(note);
        sb.append(", reNote=").append(reNote);
        sb.append(", isDel=").append(isDel);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}