package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_goods")
public class ShopGoods implements Serializable {
    /**
     * 商品ID
     */
    @Id
    private Integer id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品的货号
     */
    @Column(name = "goods_no")
    private String goodsNo;

    /**
     * 模型ID
     */
    @Column(name = "model_id")
    private Integer modelId;

    /**
     * 销售价格
     */
    @Column(name = "sell_price")
    private BigDecimal sellPrice;

    /**
     * 市场价格
     */
    @Column(name = "market_price")
    private BigDecimal marketPrice;

    /**
     * 成本价格
     */
    @Column(name = "cost_price")
    private BigDecimal costPrice;

    /**
     * 上架时间
     */
    @Column(name = "up_time")
    private Date upTime;

    /**
     * 下架时间
     */
    @Column(name = "down_time")
    private Date downTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 库存
     */
    @Column(name = "store_nums")
    private Integer storeNums;

    /**
     * 原图
     */
    private String img;

    /**
     * 宣传图
     */
    @Column(name = "ad_img")
    private String adImg;

    /**
     * 删除 0正常 1已删除 2下架 3申请上架
     */
    @Column(name = "is_del")
    private Boolean isDel;

    /**
     * SEO关键词
     */
    private String keywords;

    /**
     * SEO描述
     */
    private String description;

    /**
     * 产品搜索词库,逗号分隔
     */
    @Column(name = "search_words")
    private String searchWords;

    /**
     * 重量
     */
    private BigDecimal weight;

    /**
     * 积分
     */
    private Integer point;

    /**
     * 计量单位
     */
    private String unit;

    /**
     * 品牌ID
     */
    @Column(name = "brand_id")
    private Integer brandId;

    /**
     * 浏览次数
     */
    private Integer visit;

    /**
     * 收藏次数
     */
    private Integer favorite;

    /**
     * 排序
     */
    private Short sort;

    /**
     * 经验值
     */
    private Integer exp;

    /**
     * 评论次数
     */
    private Integer comments;

    /**
     * 销量
     */
    private Integer sale;

    /**
     * 评分总数
     */
    private Integer grade;

    /**
     * 卖家ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 共享商品 0不共享 1共享
     */
    @Column(name = "is_share")
    private Boolean isShare;

    /**
     * 商品描述
     */
    private String content;

    /**
     * 序列化存储规格,key值为规则ID，value为此商品具有的规格值
     */
    @Column(name = "spec_array")
    private String specArray;

    private static final long serialVersionUID = 1L;

    /**
     * 获取商品ID
     *
     * @return id - 商品ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置商品ID
     *
     * @param id 商品ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取商品名称
     *
     * @return name - 商品名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置商品名称
     *
     * @param name 商品名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取商品的货号
     *
     * @return goods_no - 商品的货号
     */
    public String getGoodsNo() {
        return goodsNo;
    }

    /**
     * 设置商品的货号
     *
     * @param goodsNo 商品的货号
     */
    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo == null ? null : goodsNo.trim();
    }

    /**
     * 获取模型ID
     *
     * @return model_id - 模型ID
     */
    public Integer getModelId() {
        return modelId;
    }

    /**
     * 设置模型ID
     *
     * @param modelId 模型ID
     */
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    /**
     * 获取销售价格
     *
     * @return sell_price - 销售价格
     */
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    /**
     * 设置销售价格
     *
     * @param sellPrice 销售价格
     */
    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    /**
     * 获取市场价格
     *
     * @return market_price - 市场价格
     */
    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    /**
     * 设置市场价格
     *
     * @param marketPrice 市场价格
     */
    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    /**
     * 获取成本价格
     *
     * @return cost_price - 成本价格
     */
    public BigDecimal getCostPrice() {
        return costPrice;
    }

    /**
     * 设置成本价格
     *
     * @param costPrice 成本价格
     */
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * 获取上架时间
     *
     * @return up_time - 上架时间
     */
    public Date getUpTime() {
        return upTime;
    }

    /**
     * 设置上架时间
     *
     * @param upTime 上架时间
     */
    public void setUpTime(Date upTime) {
        this.upTime = upTime;
    }

    /**
     * 获取下架时间
     *
     * @return down_time - 下架时间
     */
    public Date getDownTime() {
        return downTime;
    }

    /**
     * 设置下架时间
     *
     * @param downTime 下架时间
     */
    public void setDownTime(Date downTime) {
        this.downTime = downTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取库存
     *
     * @return store_nums - 库存
     */
    public Integer getStoreNums() {
        return storeNums;
    }

    /**
     * 设置库存
     *
     * @param storeNums 库存
     */
    public void setStoreNums(Integer storeNums) {
        this.storeNums = storeNums;
    }

    /**
     * 获取原图
     *
     * @return img - 原图
     */
    public String getImg() {
        return img;
    }

    /**
     * 设置原图
     *
     * @param img 原图
     */
    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    /**
     * 获取宣传图
     *
     * @return ad_img - 宣传图
     */
    public String getAdImg() {
        return adImg;
    }

    /**
     * 设置宣传图
     *
     * @param adImg 宣传图
     */
    public void setAdImg(String adImg) {
        this.adImg = adImg == null ? null : adImg.trim();
    }

    /**
     * 获取删除 0正常 1已删除 2下架 3申请上架
     *
     * @return is_del - 删除 0正常 1已删除 2下架 3申请上架
     */
    public Boolean getIsDel() {
        return isDel;
    }

    /**
     * 设置删除 0正常 1已删除 2下架 3申请上架
     *
     * @param isDel 删除 0正常 1已删除 2下架 3申请上架
     */
    public void setIsDel(Boolean isDel) {
        this.isDel = isDel;
    }

    /**
     * 获取SEO关键词
     *
     * @return keywords - SEO关键词
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * 设置SEO关键词
     *
     * @param keywords SEO关键词
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    /**
     * 获取SEO描述
     *
     * @return description - SEO描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置SEO描述
     *
     * @param description SEO描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 获取产品搜索词库,逗号分隔
     *
     * @return search_words - 产品搜索词库,逗号分隔
     */
    public String getSearchWords() {
        return searchWords;
    }

    /**
     * 设置产品搜索词库,逗号分隔
     *
     * @param searchWords 产品搜索词库,逗号分隔
     */
    public void setSearchWords(String searchWords) {
        this.searchWords = searchWords == null ? null : searchWords.trim();
    }

    /**
     * 获取重量
     *
     * @return weight - 重量
     */
    public BigDecimal getWeight() {
        return weight;
    }

    /**
     * 设置重量
     *
     * @param weight 重量
     */
    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    /**
     * 获取积分
     *
     * @return point - 积分
     */
    public Integer getPoint() {
        return point;
    }

    /**
     * 设置积分
     *
     * @param point 积分
     */
    public void setPoint(Integer point) {
        this.point = point;
    }

    /**
     * 获取计量单位
     *
     * @return unit - 计量单位
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 设置计量单位
     *
     * @param unit 计量单位
     */
    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    /**
     * 获取品牌ID
     *
     * @return brand_id - 品牌ID
     */
    public Integer getBrandId() {
        return brandId;
    }

    /**
     * 设置品牌ID
     *
     * @param brandId 品牌ID
     */
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    /**
     * 获取浏览次数
     *
     * @return visit - 浏览次数
     */
    public Integer getVisit() {
        return visit;
    }

    /**
     * 设置浏览次数
     *
     * @param visit 浏览次数
     */
    public void setVisit(Integer visit) {
        this.visit = visit;
    }

    /**
     * 获取收藏次数
     *
     * @return favorite - 收藏次数
     */
    public Integer getFavorite() {
        return favorite;
    }

    /**
     * 设置收藏次数
     *
     * @param favorite 收藏次数
     */
    public void setFavorite(Integer favorite) {
        this.favorite = favorite;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Short getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Short sort) {
        this.sort = sort;
    }

    /**
     * 获取经验值
     *
     * @return exp - 经验值
     */
    public Integer getExp() {
        return exp;
    }

    /**
     * 设置经验值
     *
     * @param exp 经验值
     */
    public void setExp(Integer exp) {
        this.exp = exp;
    }

    /**
     * 获取评论次数
     *
     * @return comments - 评论次数
     */
    public Integer getComments() {
        return comments;
    }

    /**
     * 设置评论次数
     *
     * @param comments 评论次数
     */
    public void setComments(Integer comments) {
        this.comments = comments;
    }

    /**
     * 获取销量
     *
     * @return sale - 销量
     */
    public Integer getSale() {
        return sale;
    }

    /**
     * 设置销量
     *
     * @param sale 销量
     */
    public void setSale(Integer sale) {
        this.sale = sale;
    }

    /**
     * 获取评分总数
     *
     * @return grade - 评分总数
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 设置评分总数
     *
     * @param grade 评分总数
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * 获取卖家ID
     *
     * @return seller_id - 卖家ID
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * 设置卖家ID
     *
     * @param sellerId 卖家ID
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * 获取共享商品 0不共享 1共享
     *
     * @return is_share - 共享商品 0不共享 1共享
     */
    public Boolean getIsShare() {
        return isShare;
    }

    /**
     * 设置共享商品 0不共享 1共享
     *
     * @param isShare 共享商品 0不共享 1共享
     */
    public void setIsShare(Boolean isShare) {
        this.isShare = isShare;
    }

    /**
     * 获取商品描述
     *
     * @return content - 商品描述
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置商品描述
     *
     * @param content 商品描述
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取序列化存储规格,key值为规则ID，value为此商品具有的规格值
     *
     * @return spec_array - 序列化存储规格,key值为规则ID，value为此商品具有的规格值
     */
    public String getSpecArray() {
        return specArray;
    }

    /**
     * 设置序列化存储规格,key值为规则ID，value为此商品具有的规格值
     *
     * @param specArray 序列化存储规格,key值为规则ID，value为此商品具有的规格值
     */
    public void setSpecArray(String specArray) {
        this.specArray = specArray == null ? null : specArray.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", goodsNo=").append(goodsNo);
        sb.append(", modelId=").append(modelId);
        sb.append(", sellPrice=").append(sellPrice);
        sb.append(", marketPrice=").append(marketPrice);
        sb.append(", costPrice=").append(costPrice);
        sb.append(", upTime=").append(upTime);
        sb.append(", downTime=").append(downTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", storeNums=").append(storeNums);
        sb.append(", img=").append(img);
        sb.append(", adImg=").append(adImg);
        sb.append(", isDel=").append(isDel);
        sb.append(", keywords=").append(keywords);
        sb.append(", description=").append(description);
        sb.append(", searchWords=").append(searchWords);
        sb.append(", weight=").append(weight);
        sb.append(", point=").append(point);
        sb.append(", unit=").append(unit);
        sb.append(", brandId=").append(brandId);
        sb.append(", visit=").append(visit);
        sb.append(", favorite=").append(favorite);
        sb.append(", sort=").append(sort);
        sb.append(", exp=").append(exp);
        sb.append(", comments=").append(comments);
        sb.append(", sale=").append(sale);
        sb.append(", grade=").append(grade);
        sb.append(", sellerId=").append(sellerId);
        sb.append(", isShare=").append(isShare);
        sb.append(", content=").append(content);
        sb.append(", specArray=").append(specArray);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}