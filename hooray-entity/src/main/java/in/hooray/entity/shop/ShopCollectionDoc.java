package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_collection_doc")
public class ShopCollectionDoc implements Serializable {
    @Id
    private Integer id;

    /**
     * 订单号
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 时间
     */
    private Date time;

    /**
     * 支付方式ID
     */
    @Column(name = "payment_id")
    private Integer paymentId;

    /**
     * 管理员id
     */
    @Column(name = "admin_id")
    private Integer adminId;

    /**
     * 支付状态，0:准备，1:支付成功
     */
    @Column(name = "pay_status")
    private Boolean payStatus;

    /**
     * 0:未删除 1:删除
     */
    @Column(name = "if_del")
    private Boolean ifDel;

    /**
     * 收款备注
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单号
     *
     * @return order_id - 订单号
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单号
     *
     * @param orderId 订单号
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取金额
     *
     * @return amount - 金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置金额
     *
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取时间
     *
     * @return time - 时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取支付方式ID
     *
     * @return payment_id - 支付方式ID
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * 设置支付方式ID
     *
     * @param paymentId 支付方式ID
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * 获取管理员id
     *
     * @return admin_id - 管理员id
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 设置管理员id
     *
     * @param adminId 管理员id
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取支付状态，0:准备，1:支付成功
     *
     * @return pay_status - 支付状态，0:准备，1:支付成功
     */
    public Boolean getPayStatus() {
        return payStatus;
    }

    /**
     * 设置支付状态，0:准备，1:支付成功
     *
     * @param payStatus 支付状态，0:准备，1:支付成功
     */
    public void setPayStatus(Boolean payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 获取0:未删除 1:删除
     *
     * @return if_del - 0:未删除 1:删除
     */
    public Boolean getIfDel() {
        return ifDel;
    }

    /**
     * 设置0:未删除 1:删除
     *
     * @param ifDel 0:未删除 1:删除
     */
    public void setIfDel(Boolean ifDel) {
        this.ifDel = ifDel;
    }

    /**
     * 获取收款备注
     *
     * @return note - 收款备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置收款备注
     *
     * @param note 收款备注
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", userId=").append(userId);
        sb.append(", amount=").append(amount);
        sb.append(", time=").append(time);
        sb.append(", paymentId=").append(paymentId);
        sb.append(", adminId=").append(adminId);
        sb.append(", payStatus=").append(payStatus);
        sb.append(", ifDel=").append(ifDel);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}