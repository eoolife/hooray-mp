package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_model")
public class ShopModel implements Serializable {
    /**
     * 模型ID
     */
    @Id
    private Integer id;

    /**
     * 模型名称
     */
    private String name;

    /**
     * 规格ID逗号分隔
     */
    @Column(name = "spec_ids")
    private String specIds;

    private static final long serialVersionUID = 1L;

    /**
     * 获取模型ID
     *
     * @return id - 模型ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置模型ID
     *
     * @param id 模型ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取模型名称
     *
     * @return name - 模型名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置模型名称
     *
     * @param name 模型名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取规格ID逗号分隔
     *
     * @return spec_ids - 规格ID逗号分隔
     */
    public String getSpecIds() {
        return specIds;
    }

    /**
     * 设置规格ID逗号分隔
     *
     * @param specIds 规格ID逗号分隔
     */
    public void setSpecIds(String specIds) {
        this.specIds = specIds == null ? null : specIds.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", specIds=").append(specIds);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}