package in.hooray.entity.shop;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "shop_delivery_doc")
public class ShopDeliveryDoc implements Serializable {
    /**
     * 发货单ID
     */
    @Id
    private Integer id;

    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Integer orderId;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 管理员ID
     */
    @Column(name = "admin_id")
    private Integer adminId;

    /**
     * 商户ID
     */
    @Column(name = "seller_id")
    private Integer sellerId;

    /**
     * 收货人
     */
    private String name;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 联系电话
     */
    private String telphone;

    /**
     * 国ID
     */
    private Integer country;

    /**
     * 省ID
     */
    private Integer province;

    /**
     * 市ID
     */
    private Integer city;

    /**
     * 区ID
     */
    private Integer area;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 创建时间
     */
    private Date time;

    /**
     * 运费
     */
    private BigDecimal freight;

    /**
     * 物流单号
     */
    @Column(name = "delivery_code")
    private String deliveryCode;

    /**
     * 物流方式
     */
    @Column(name = "delivery_type")
    private String deliveryType;

    /**
     * 0:未删除 1:已删除
     */
    @Column(name = "if_del")
    private Boolean ifDel;

    /**
     * 货运公司ID
     */
    @Column(name = "freight_id")
    private Integer freightId;

    /**
     * 管理员添加的备注信息
     */
    private String note;

    private static final long serialVersionUID = 1L;

    /**
     * 获取发货单ID
     *
     * @return id - 发货单ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置发货单ID
     *
     * @param id 发货单ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取订单ID
     *
     * @return order_id - 订单ID
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId 订单ID
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 获取用户ID
     *
     * @return user_id - 用户ID
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置用户ID
     *
     * @param userId 用户ID
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取管理员ID
     *
     * @return admin_id - 管理员ID
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * 设置管理员ID
     *
     * @param adminId 管理员ID
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * 获取商户ID
     *
     * @return seller_id - 商户ID
     */
    public Integer getSellerId() {
        return sellerId;
    }

    /**
     * 设置商户ID
     *
     * @param sellerId 商户ID
     */
    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    /**
     * 获取收货人
     *
     * @return name - 收货人
     */
    public String getName() {
        return name;
    }

    /**
     * 设置收货人
     *
     * @param name 收货人
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取邮编
     *
     * @return postcode - 邮编
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 设置邮编
     *
     * @param postcode 邮编
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    /**
     * 获取联系电话
     *
     * @return telphone - 联系电话
     */
    public String getTelphone() {
        return telphone;
    }

    /**
     * 设置联系电话
     *
     * @param telphone 联系电话
     */
    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    /**
     * 获取国ID
     *
     * @return country - 国ID
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * 设置国ID
     *
     * @param country 国ID
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * 获取省ID
     *
     * @return province - 省ID
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * 设置省ID
     *
     * @param province 省ID
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * 获取市ID
     *
     * @return city - 市ID
     */
    public Integer getCity() {
        return city;
    }

    /**
     * 设置市ID
     *
     * @param city 市ID
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * 获取区ID
     *
     * @return area - 区ID
     */
    public Integer getArea() {
        return area;
    }

    /**
     * 设置区ID
     *
     * @param area 区ID
     */
    public void setArea(Integer area) {
        this.area = area;
    }

    /**
     * 获取收货地址
     *
     * @return address - 收货地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置收货地址
     *
     * @param address 收货地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取手机
     *
     * @return mobile - 手机
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机
     *
     * @param mobile 手机
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取创建时间
     *
     * @return time - 创建时间
     */
    public Date getTime() {
        return time;
    }

    /**
     * 设置创建时间
     *
     * @param time 创建时间
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * 获取运费
     *
     * @return freight - 运费
     */
    public BigDecimal getFreight() {
        return freight;
    }

    /**
     * 设置运费
     *
     * @param freight 运费
     */
    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    /**
     * 获取物流单号
     *
     * @return delivery_code - 物流单号
     */
    public String getDeliveryCode() {
        return deliveryCode;
    }

    /**
     * 设置物流单号
     *
     * @param deliveryCode 物流单号
     */
    public void setDeliveryCode(String deliveryCode) {
        this.deliveryCode = deliveryCode == null ? null : deliveryCode.trim();
    }

    /**
     * 获取物流方式
     *
     * @return delivery_type - 物流方式
     */
    public String getDeliveryType() {
        return deliveryType;
    }

    /**
     * 设置物流方式
     *
     * @param deliveryType 物流方式
     */
    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType == null ? null : deliveryType.trim();
    }

    /**
     * 获取0:未删除 1:已删除
     *
     * @return if_del - 0:未删除 1:已删除
     */
    public Boolean getIfDel() {
        return ifDel;
    }

    /**
     * 设置0:未删除 1:已删除
     *
     * @param ifDel 0:未删除 1:已删除
     */
    public void setIfDel(Boolean ifDel) {
        this.ifDel = ifDel;
    }

    /**
     * 获取货运公司ID
     *
     * @return freight_id - 货运公司ID
     */
    public Integer getFreightId() {
        return freightId;
    }

    /**
     * 设置货运公司ID
     *
     * @param freightId 货运公司ID
     */
    public void setFreightId(Integer freightId) {
        this.freightId = freightId;
    }

    /**
     * 获取管理员添加的备注信息
     *
     * @return note - 管理员添加的备注信息
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置管理员添加的备注信息
     *
     * @param note 管理员添加的备注信息
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", userId=").append(userId);
        sb.append(", adminId=").append(adminId);
        sb.append(", sellerId=").append(sellerId);
        sb.append(", name=").append(name);
        sb.append(", postcode=").append(postcode);
        sb.append(", telphone=").append(telphone);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", area=").append(area);
        sb.append(", address=").append(address);
        sb.append(", mobile=").append(mobile);
        sb.append(", time=").append(time);
        sb.append(", freight=").append(freight);
        sb.append(", deliveryCode=").append(deliveryCode);
        sb.append(", deliveryType=").append(deliveryType);
        sb.append(", ifDel=").append(ifDel);
        sb.append(", freightId=").append(freightId);
        sb.append(", note=").append(note);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}