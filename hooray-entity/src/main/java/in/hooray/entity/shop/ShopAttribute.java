package in.hooray.entity.shop;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "shop_attribute")
public class ShopAttribute implements Serializable {
    /**
     * 属性ID
     */
    @Id
    private Integer id;

    /**
     * 模型ID
     */
    @Column(name = "model_id")
    private Integer modelId;

    /**
     * 输入控件的类型,1:单选,2:复选,3:下拉,4:输入框
     */
    private Boolean type;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否支持搜索0不支持1支持
     */
    private Boolean search;

    /**
     * 属性值(逗号分隔)
     */
    private String value;

    private static final long serialVersionUID = 1L;

    /**
     * 获取属性ID
     *
     * @return id - 属性ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置属性ID
     *
     * @param id 属性ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取模型ID
     *
     * @return model_id - 模型ID
     */
    public Integer getModelId() {
        return modelId;
    }

    /**
     * 设置模型ID
     *
     * @param modelId 模型ID
     */
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    /**
     * 获取输入控件的类型,1:单选,2:复选,3:下拉,4:输入框
     *
     * @return type - 输入控件的类型,1:单选,2:复选,3:下拉,4:输入框
     */
    public Boolean getType() {
        return type;
    }

    /**
     * 设置输入控件的类型,1:单选,2:复选,3:下拉,4:输入框
     *
     * @param type 输入控件的类型,1:单选,2:复选,3:下拉,4:输入框
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取是否支持搜索0不支持1支持
     *
     * @return search - 是否支持搜索0不支持1支持
     */
    public Boolean getSearch() {
        return search;
    }

    /**
     * 设置是否支持搜索0不支持1支持
     *
     * @param search 是否支持搜索0不支持1支持
     */
    public void setSearch(Boolean search) {
        this.search = search;
    }

    /**
     * 获取属性值(逗号分隔)
     *
     * @return value - 属性值(逗号分隔)
     */
    public String getValue() {
        return value;
    }

    /**
     * 设置属性值(逗号分隔)
     *
     * @param value 属性值(逗号分隔)
     */
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", modelId=").append(modelId);
        sb.append(", type=").append(type);
        sb.append(", name=").append(name);
        sb.append(", search=").append(search);
        sb.append(", value=").append(value);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}