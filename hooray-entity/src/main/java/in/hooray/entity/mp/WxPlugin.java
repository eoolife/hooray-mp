package in.hooray.entity.mp;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "wx_plugin")
public class WxPlugin implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 插件名称
     */
    private String name;

    private String clazzpath;

    private Boolean status;

    private String remark;

    private static final long serialVersionUID = 1L;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取插件名称
     *
     * @return name - 插件名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置插件名称
     *
     * @param name 插件名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return clazzpath
     */
    public String getClazzpath() {
        return clazzpath;
    }

    /**
     * @param clazzpath
     */
    public void setClazzpath(String clazzpath) {
        this.clazzpath = clazzpath == null ? null : clazzpath.trim();
    }

    /**
     * @return status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", clazzpath=").append(clazzpath);
        sb.append(", status=").append(status);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}