package in.hooray.entity.mp;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_account_wechats")
public class AccountWechat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer acid;

    private Integer uniacid;

    private String token;

    private Byte level;

    private String name;

    /**
     * 微信公众号的微信帐号
     */
    @Column(name = "wx_account")
    private String wxAccount;

    /**
     * 行业代码
     */
    @Column(name = "industry_ids")
    private String industryIds;

    /**
     * 公众号原始ID
     */
    private String original;

    /**
     * 首次关注回复内容
     */
    private String signature;

    /**
     * 公众号联系地址
     */
    private String address;

    /**
     * 该公众号的联系人
     */
    private String linkman;

    /**
     * 该公众号的联系人电话
     */
    private String linkphone;

    private String appid;

    private String secret;

    private String aeskey;

    private String qrcodeurl;

    private Date addtime;

    private Integer styleid;

    private static final long serialVersionUID = 1L;

    /**
     * @return acid
     */
    public Integer getAcid() {
        return acid;
    }

    /**
     * @param acid
     */
    public void setAcid(Integer acid) {
        this.acid = acid;
    }

    /**
     * @return uniacid
     */
    public Integer getUniacid() {
        return uniacid;
    }

    /**
     * @param uniacid
     */
    public void setUniacid(Integer uniacid) {
        this.uniacid = uniacid;
    }

    /**
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     */
    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    /**
     * @return level
     */
    public Byte getLevel() {
        return level;
    }

    /**
     * @param level
     */
    public void setLevel(Byte level) {
        this.level = level;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取微信公众号的微信帐号
     *
     * @return wx_account - 微信公众号的微信帐号
     */
    public String getWxAccount() {
        return wxAccount;
    }

    /**
     * 设置微信公众号的微信帐号
     *
     * @param wxAccount 微信公众号的微信帐号
     */
    public void setWxAccount(String wxAccount) {
        this.wxAccount = wxAccount == null ? null : wxAccount.trim();
    }

    /**
     * 获取行业代码
     *
     * @return industry_ids - 行业代码
     */
    public String getIndustryIds() {
        return industryIds;
    }

    /**
     * 设置行业代码
     *
     * @param industryIds 行业代码
     */
    public void setIndustryIds(String industryIds) {
        this.industryIds = industryIds == null ? null : industryIds.trim();
    }

    /**
     * 获取公众号原始ID
     *
     * @return original - 公众号原始ID
     */
    public String getOriginal() {
        return original;
    }

    /**
     * 设置公众号原始ID
     *
     * @param original 公众号原始ID
     */
    public void setOriginal(String original) {
        this.original = original == null ? null : original.trim();
    }

    /**
     * 获取首次关注回复内容
     *
     * @return signature - 首次关注回复内容
     */
    public String getSignature() {
        return signature;
    }

    /**
     * 设置首次关注回复内容
     *
     * @param signature 首次关注回复内容
     */
    public void setSignature(String signature) {
        this.signature = signature == null ? null : signature.trim();
    }

    /**
     * 获取公众号联系地址
     *
     * @return address - 公众号联系地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置公众号联系地址
     *
     * @param address 公众号联系地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取该公众号的联系人
     *
     * @return linkman - 该公众号的联系人
     */
    public String getLinkman() {
        return linkman;
    }

    /**
     * 设置该公众号的联系人
     *
     * @param linkman 该公众号的联系人
     */
    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    /**
     * 获取该公众号的联系人电话
     *
     * @return linkphone - 该公众号的联系人电话
     */
    public String getLinkphone() {
        return linkphone;
    }

    /**
     * 设置该公众号的联系人电话
     *
     * @param linkphone 该公众号的联系人电话
     */
    public void setLinkphone(String linkphone) {
        this.linkphone = linkphone == null ? null : linkphone.trim();
    }

    /**
     * @return appid
     */
    public String getAppid() {
        return appid;
    }

    /**
     * @param appid
     */
    public void setAppid(String appid) {
        this.appid = appid == null ? null : appid.trim();
    }

    /**
     * @return secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * @param secret
     */
    public void setSecret(String secret) {
        this.secret = secret == null ? null : secret.trim();
    }

    /**
     * @return aeskey
     */
    public String getAeskey() {
        return aeskey;
    }

    /**
     * @param aeskey
     */
    public void setAeskey(String aeskey) {
        this.aeskey = aeskey == null ? null : aeskey.trim();
    }

    /**
     * @return qrcodeurl
     */
    public String getQrcodeurl() {
        return qrcodeurl;
    }

    /**
     * @param qrcodeurl
     */
    public void setQrcodeurl(String qrcodeurl) {
        this.qrcodeurl = qrcodeurl == null ? null : qrcodeurl.trim();
    }

    /**
     * @return addtime
     */
    public Date getAddtime() {
        return addtime;
    }

    /**
     * @param addtime
     */
    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    /**
     * @return styleid
     */
    public Integer getStyleid() {
        return styleid;
    }

    /**
     * @param styleid
     */
    public void setStyleid(Integer styleid) {
        this.styleid = styleid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", acid=").append(acid);
        sb.append(", uniacid=").append(uniacid);
        sb.append(", token=").append(token);
        sb.append(", level=").append(level);
        sb.append(", name=").append(name);
        sb.append(", wxAccount=").append(wxAccount);
        sb.append(", industryIds=").append(industryIds);
        sb.append(", original=").append(original);
        sb.append(", signature=").append(signature);
        sb.append(", address=").append(address);
        sb.append(", linkman=").append(linkman);
        sb.append(", linkphone=").append(linkphone);
        sb.append(", appid=").append(appid);
        sb.append(", secret=").append(secret);
        sb.append(", aeskey=").append(aeskey);
        sb.append(", qrcodeurl=").append(qrcodeurl);
        sb.append(", addtime=").append(addtime);
        sb.append(", styleid=").append(styleid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}