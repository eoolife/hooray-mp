package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopExpresswayBill;
import in.hooray.entity.shop.ShopExpresswayBillCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopExpresswayBillMapper extends MyMapper<ShopExpresswayBill> {
    int countByExample(ShopExpresswayBillCriteria example);

    int deleteByExample(ShopExpresswayBillCriteria example);

    List<ShopExpresswayBill> selectByExampleWithBLOBsWithRowbounds(ShopExpresswayBillCriteria example, RowBounds rowBounds);

    List<ShopExpresswayBill> selectByExampleWithBLOBs(ShopExpresswayBillCriteria example);

    List<ShopExpresswayBill> selectByExampleWithRowbounds(ShopExpresswayBillCriteria example, RowBounds rowBounds);

    List<ShopExpresswayBill> selectByExample(ShopExpresswayBillCriteria example);

    int updateByExampleSelective(@Param("record") ShopExpresswayBill record, @Param("example") ShopExpresswayBillCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopExpresswayBill record, @Param("example") ShopExpresswayBillCriteria example);

    int updateByExample(@Param("record") ShopExpresswayBill record, @Param("example") ShopExpresswayBillCriteria example);
}