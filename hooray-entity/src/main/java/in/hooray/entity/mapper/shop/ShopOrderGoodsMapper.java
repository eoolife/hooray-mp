package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopOrderGoods;
import in.hooray.entity.shop.ShopOrderGoodsCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopOrderGoodsMapper extends MyMapper<ShopOrderGoods> {
    int countByExample(ShopOrderGoodsCriteria example);

    int deleteByExample(ShopOrderGoodsCriteria example);

    List<ShopOrderGoods> selectByExampleWithBLOBsWithRowbounds(ShopOrderGoodsCriteria example, RowBounds rowBounds);

    List<ShopOrderGoods> selectByExampleWithBLOBs(ShopOrderGoodsCriteria example);

    List<ShopOrderGoods> selectByExampleWithRowbounds(ShopOrderGoodsCriteria example, RowBounds rowBounds);

    List<ShopOrderGoods> selectByExample(ShopOrderGoodsCriteria example);

    int updateByExampleSelective(@Param("record") ShopOrderGoods record, @Param("example") ShopOrderGoodsCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopOrderGoods record, @Param("example") ShopOrderGoodsCriteria example);

    int updateByExample(@Param("record") ShopOrderGoods record, @Param("example") ShopOrderGoodsCriteria example);
}