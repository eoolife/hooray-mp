package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopFreightCompany;
import in.hooray.entity.shop.ShopFreightCompanyCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopFreightCompanyMapper extends MyMapper<ShopFreightCompany> {
    int countByExample(ShopFreightCompanyCriteria example);

    int deleteByExample(ShopFreightCompanyCriteria example);

    List<ShopFreightCompany> selectByExampleWithRowbounds(ShopFreightCompanyCriteria example, RowBounds rowBounds);

    List<ShopFreightCompany> selectByExample(ShopFreightCompanyCriteria example);

    int updateByExampleSelective(@Param("record") ShopFreightCompany record, @Param("example") ShopFreightCompanyCriteria example);

    int updateByExample(@Param("record") ShopFreightCompany record, @Param("example") ShopFreightCompanyCriteria example);
}