package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopDelivery;
import in.hooray.entity.shop.ShopDeliveryCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopDeliveryMapper extends MyMapper<ShopDelivery> {
    int countByExample(ShopDeliveryCriteria example);

    int deleteByExample(ShopDeliveryCriteria example);

    List<ShopDelivery> selectByExampleWithBLOBsWithRowbounds(ShopDeliveryCriteria example, RowBounds rowBounds);

    List<ShopDelivery> selectByExampleWithBLOBs(ShopDeliveryCriteria example);

    List<ShopDelivery> selectByExampleWithRowbounds(ShopDeliveryCriteria example, RowBounds rowBounds);

    List<ShopDelivery> selectByExample(ShopDeliveryCriteria example);

    int updateByExampleSelective(@Param("record") ShopDelivery record, @Param("example") ShopDeliveryCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopDelivery record, @Param("example") ShopDeliveryCriteria example);

    int updateByExample(@Param("record") ShopDelivery record, @Param("example") ShopDeliveryCriteria example);
}