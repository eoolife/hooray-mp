package in.hooray.entity.mapper;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.mp.WxPlugin;
import in.hooray.entity.mp.WxPluginCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface WxPluginMapper extends MyMapper<WxPlugin> {
    int countByExample(WxPluginCriteria example);

    int deleteByExample(WxPluginCriteria example);

    List<WxPlugin> selectByExampleWithBLOBsWithRowbounds(WxPluginCriteria example, RowBounds rowBounds);

    List<WxPlugin> selectByExampleWithBLOBs(WxPluginCriteria example);

    List<WxPlugin> selectByExampleWithRowbounds(WxPluginCriteria example, RowBounds rowBounds);

    List<WxPlugin> selectByExample(WxPluginCriteria example);

    int updateByExampleSelective(@Param("record") WxPlugin record, @Param("example") WxPluginCriteria example);

    int updateByExampleWithBLOBs(@Param("record") WxPlugin record, @Param("example") WxPluginCriteria example);

    int updateByExample(@Param("record") WxPlugin record, @Param("example") WxPluginCriteria example);
}