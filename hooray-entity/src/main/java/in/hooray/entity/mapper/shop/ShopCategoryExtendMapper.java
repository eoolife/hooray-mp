package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopCategoryExtend;
import in.hooray.entity.shop.ShopCategoryExtendCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopCategoryExtendMapper extends MyMapper<ShopCategoryExtend> {
    int countByExample(ShopCategoryExtendCriteria example);

    int deleteByExample(ShopCategoryExtendCriteria example);

    List<ShopCategoryExtend> selectByExampleWithRowbounds(ShopCategoryExtendCriteria example, RowBounds rowBounds);

    List<ShopCategoryExtend> selectByExample(ShopCategoryExtendCriteria example);

    int updateByExampleSelective(@Param("record") ShopCategoryExtend record, @Param("example") ShopCategoryExtendCriteria example);

    int updateByExample(@Param("record") ShopCategoryExtend record, @Param("example") ShopCategoryExtendCriteria example);
}