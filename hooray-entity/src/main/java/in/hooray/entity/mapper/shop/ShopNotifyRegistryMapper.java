package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopNotifyRegistry;
import in.hooray.entity.shop.ShopNotifyRegistryCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopNotifyRegistryMapper extends MyMapper<ShopNotifyRegistry> {
    int countByExample(ShopNotifyRegistryCriteria example);

    int deleteByExample(ShopNotifyRegistryCriteria example);

    List<ShopNotifyRegistry> selectByExampleWithRowbounds(ShopNotifyRegistryCriteria example, RowBounds rowBounds);

    List<ShopNotifyRegistry> selectByExample(ShopNotifyRegistryCriteria example);

    int updateByExampleSelective(@Param("record") ShopNotifyRegistry record, @Param("example") ShopNotifyRegistryCriteria example);

    int updateByExample(@Param("record") ShopNotifyRegistry record, @Param("example") ShopNotifyRegistryCriteria example);
}