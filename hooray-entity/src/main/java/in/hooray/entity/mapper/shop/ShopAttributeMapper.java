package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopAttribute;
import in.hooray.entity.shop.ShopAttributeCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopAttributeMapper extends MyMapper<ShopAttribute> {
    int countByExample(ShopAttributeCriteria example);

    int deleteByExample(ShopAttributeCriteria example);

    List<ShopAttribute> selectByExampleWithBLOBsWithRowbounds(ShopAttributeCriteria example, RowBounds rowBounds);

    List<ShopAttribute> selectByExampleWithBLOBs(ShopAttributeCriteria example);

    List<ShopAttribute> selectByExampleWithRowbounds(ShopAttributeCriteria example, RowBounds rowBounds);

    List<ShopAttribute> selectByExample(ShopAttributeCriteria example);

    int updateByExampleSelective(@Param("record") ShopAttribute record, @Param("example") ShopAttributeCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopAttribute record, @Param("example") ShopAttributeCriteria example);

    int updateByExample(@Param("record") ShopAttribute record, @Param("example") ShopAttributeCriteria example);
}