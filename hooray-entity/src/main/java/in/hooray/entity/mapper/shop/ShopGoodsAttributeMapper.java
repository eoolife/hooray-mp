package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGoodsAttribute;
import in.hooray.entity.shop.ShopGoodsAttributeCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGoodsAttributeMapper extends MyMapper<ShopGoodsAttribute> {
    int countByExample(ShopGoodsAttributeCriteria example);

    int deleteByExample(ShopGoodsAttributeCriteria example);

    List<ShopGoodsAttribute> selectByExampleWithRowbounds(ShopGoodsAttributeCriteria example, RowBounds rowBounds);

    List<ShopGoodsAttribute> selectByExample(ShopGoodsAttributeCriteria example);

    int updateByExampleSelective(@Param("record") ShopGoodsAttribute record, @Param("example") ShopGoodsAttributeCriteria example);

    int updateByExample(@Param("record") ShopGoodsAttribute record, @Param("example") ShopGoodsAttributeCriteria example);
}