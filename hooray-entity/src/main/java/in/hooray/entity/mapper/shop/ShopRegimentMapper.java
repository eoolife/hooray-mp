package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopRegiment;
import in.hooray.entity.shop.ShopRegimentCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopRegimentMapper extends MyMapper<ShopRegiment> {
    int countByExample(ShopRegimentCriteria example);

    int deleteByExample(ShopRegimentCriteria example);

    List<ShopRegiment> selectByExampleWithRowbounds(ShopRegimentCriteria example, RowBounds rowBounds);

    List<ShopRegiment> selectByExample(ShopRegimentCriteria example);

    int updateByExampleSelective(@Param("record") ShopRegiment record, @Param("example") ShopRegimentCriteria example);

    int updateByExample(@Param("record") ShopRegiment record, @Param("example") ShopRegimentCriteria example);
}