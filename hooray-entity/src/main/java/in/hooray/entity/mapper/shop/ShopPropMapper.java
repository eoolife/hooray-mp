package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopProp;
import in.hooray.entity.shop.ShopPropCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopPropMapper extends MyMapper<ShopProp> {
    int countByExample(ShopPropCriteria example);

    int deleteByExample(ShopPropCriteria example);

    List<ShopProp> selectByExampleWithRowbounds(ShopPropCriteria example, RowBounds rowBounds);

    List<ShopProp> selectByExample(ShopPropCriteria example);

    int updateByExampleSelective(@Param("record") ShopProp record, @Param("example") ShopPropCriteria example);

    int updateByExample(@Param("record") ShopProp record, @Param("example") ShopPropCriteria example);
}