package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopOnlineRecharge;
import in.hooray.entity.shop.ShopOnlineRechargeCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopOnlineRechargeMapper extends MyMapper<ShopOnlineRecharge> {
    int countByExample(ShopOnlineRechargeCriteria example);

    int deleteByExample(ShopOnlineRechargeCriteria example);

    List<ShopOnlineRecharge> selectByExampleWithRowbounds(ShopOnlineRechargeCriteria example, RowBounds rowBounds);

    List<ShopOnlineRecharge> selectByExample(ShopOnlineRechargeCriteria example);

    int updateByExampleSelective(@Param("record") ShopOnlineRecharge record, @Param("example") ShopOnlineRechargeCriteria example);

    int updateByExample(@Param("record") ShopOnlineRecharge record, @Param("example") ShopOnlineRechargeCriteria example);
}