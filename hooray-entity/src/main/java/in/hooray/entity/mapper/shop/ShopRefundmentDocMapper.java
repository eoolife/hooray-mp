package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopRefundmentDoc;
import in.hooray.entity.shop.ShopRefundmentDocCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopRefundmentDocMapper extends MyMapper<ShopRefundmentDoc> {
    int countByExample(ShopRefundmentDocCriteria example);

    int deleteByExample(ShopRefundmentDocCriteria example);

    List<ShopRefundmentDoc> selectByExampleWithBLOBsWithRowbounds(ShopRefundmentDocCriteria example, RowBounds rowBounds);

    List<ShopRefundmentDoc> selectByExampleWithBLOBs(ShopRefundmentDocCriteria example);

    List<ShopRefundmentDoc> selectByExampleWithRowbounds(ShopRefundmentDocCriteria example, RowBounds rowBounds);

    List<ShopRefundmentDoc> selectByExample(ShopRefundmentDocCriteria example);

    int updateByExampleSelective(@Param("record") ShopRefundmentDoc record, @Param("example") ShopRefundmentDocCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopRefundmentDoc record, @Param("example") ShopRefundmentDocCriteria example);

    int updateByExample(@Param("record") ShopRefundmentDoc record, @Param("example") ShopRefundmentDocCriteria example);
}