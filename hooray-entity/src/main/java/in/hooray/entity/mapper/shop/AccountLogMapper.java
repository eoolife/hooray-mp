package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.AccountLog;
import in.hooray.entity.shop.AccountLogCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface AccountLogMapper extends MyMapper<AccountLog> {
    int countByExample(AccountLogCriteria example);

    int deleteByExample(AccountLogCriteria example);

    List<AccountLog> selectByExampleWithBLOBsWithRowbounds(AccountLogCriteria example, RowBounds rowBounds);

    List<AccountLog> selectByExampleWithBLOBs(AccountLogCriteria example);

    List<AccountLog> selectByExampleWithRowbounds(AccountLogCriteria example, RowBounds rowBounds);

    List<AccountLog> selectByExample(AccountLogCriteria example);

    int updateByExampleSelective(@Param("record") AccountLog record, @Param("example") AccountLogCriteria example);

    int updateByExampleWithBLOBs(@Param("record") AccountLog record, @Param("example") AccountLogCriteria example);

    int updateByExample(@Param("record") AccountLog record, @Param("example") AccountLogCriteria example);
}