package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopGroupPrice;
import in.hooray.entity.shop.ShopGroupPriceCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopGroupPriceMapper extends MyMapper<ShopGroupPrice> {
    int countByExample(ShopGroupPriceCriteria example);

    int deleteByExample(ShopGroupPriceCriteria example);

    List<ShopGroupPrice> selectByExampleWithRowbounds(ShopGroupPriceCriteria example, RowBounds rowBounds);

    List<ShopGroupPrice> selectByExample(ShopGroupPriceCriteria example);

    int updateByExampleSelective(@Param("record") ShopGroupPrice record, @Param("example") ShopGroupPriceCriteria example);

    int updateByExample(@Param("record") ShopGroupPrice record, @Param("example") ShopGroupPriceCriteria example);
}