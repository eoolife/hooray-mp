package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopSpecPhoto;
import in.hooray.entity.shop.ShopSpecPhotoCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopSpecPhotoMapper extends MyMapper<ShopSpecPhoto> {
    int countByExample(ShopSpecPhotoCriteria example);

    int deleteByExample(ShopSpecPhotoCriteria example);

    List<ShopSpecPhoto> selectByExampleWithRowbounds(ShopSpecPhotoCriteria example, RowBounds rowBounds);

    List<ShopSpecPhoto> selectByExample(ShopSpecPhotoCriteria example);

    int updateByExampleSelective(@Param("record") ShopSpecPhoto record, @Param("example") ShopSpecPhotoCriteria example);

    int updateByExample(@Param("record") ShopSpecPhoto record, @Param("example") ShopSpecPhotoCriteria example);
}