package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopWithdraw;
import in.hooray.entity.shop.ShopWithdrawCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopWithdrawMapper extends MyMapper<ShopWithdraw> {
    int countByExample(ShopWithdrawCriteria example);

    int deleteByExample(ShopWithdrawCriteria example);

    List<ShopWithdraw> selectByExampleWithRowbounds(ShopWithdrawCriteria example, RowBounds rowBounds);

    List<ShopWithdraw> selectByExample(ShopWithdrawCriteria example);

    int updateByExampleSelective(@Param("record") ShopWithdraw record, @Param("example") ShopWithdrawCriteria example);

    int updateByExample(@Param("record") ShopWithdraw record, @Param("example") ShopWithdrawCriteria example);
}