package in.hooray.entity.mapper.shop;

import in.hooray.core.mybatis.plugin.MyMapper;
import in.hooray.entity.shop.ShopModel;
import in.hooray.entity.shop.ShopModelCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface ShopModelMapper extends MyMapper<ShopModel> {
    int countByExample(ShopModelCriteria example);

    int deleteByExample(ShopModelCriteria example);

    List<ShopModel> selectByExampleWithBLOBsWithRowbounds(ShopModelCriteria example, RowBounds rowBounds);

    List<ShopModel> selectByExampleWithBLOBs(ShopModelCriteria example);

    List<ShopModel> selectByExampleWithRowbounds(ShopModelCriteria example, RowBounds rowBounds);

    List<ShopModel> selectByExample(ShopModelCriteria example);

    int updateByExampleSelective(@Param("record") ShopModel record, @Param("example") ShopModelCriteria example);

    int updateByExampleWithBLOBs(@Param("record") ShopModel record, @Param("example") ShopModelCriteria example);

    int updateByExample(@Param("record") ShopModel record, @Param("example") ShopModelCriteria example);
}