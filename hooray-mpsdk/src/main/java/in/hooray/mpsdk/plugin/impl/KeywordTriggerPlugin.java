package in.hooray.mpsdk.plugin.impl;

import org.springframework.stereotype.Component;

import in.hooray.mpsdk.plugin.IPlugin;
import in.hooray.mpsdk.plugin.PluginInfo;
import in.hooray.mpsdk.plugin.PluginParam;

/**
 * 关键字触发插件，可定义关键字回复内容或仅是收集
 * @author darking
 *
 */
@Component
public class KeywordTriggerPlugin implements IPlugin {

	@Override
	public Object execute(PluginParam param) {
		
		return null;
	}

	@Override
	public PluginInfo getPluginInfo(String pluginName) {
		return null;
	}
	
}
