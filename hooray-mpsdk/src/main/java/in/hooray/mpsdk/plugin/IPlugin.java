package in.hooray.mpsdk.plugin;

/**
 * 微信服务插件接口
 * @author darking
 *
 */
public interface IPlugin {

	Object execute(PluginParam param);
	
	PluginInfo getPluginInfo(String pluginName);
	
}
