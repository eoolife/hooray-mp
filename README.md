#hooray-mp(一套基于SpringBoot上的开发基础集成)

PS：`一开始打算做微信平台的，所以命名也是延续了微信mp的习惯，不改了，一般人下载用都会改自己喜欢的项目名和包命名风格`

QQ群：554750593 

![springbootqq](./doc/images/springbootqq.png)

### TODO
    1. ``Mybatis Spring Boot 1.0.1`` 正式版出炉了，项目里需要集成....满满的都是工作量

一个基于Spring Boot之上的BaaS（后台即服务）开发套件，每个模块可独立开发，相互之间可通过HTTP Restful或者JMS之类的队列进行数据交互。该项目不会是完整的项目，只是提供一种思路，
做个技术栈整合，方便自己架构时选择各层合适的技术。


独立的运行的项目，可被hooray-sadmin进行监控及管理，即开发自运维，让你时刻了解应用服务的各个方面，性能调优，接口方法被调用的Metric指标等，
摆脱曾经那一个电脑十多个SSH监控十多台服务器的土鳖时代

可方便集成Maven+SonarQube+Jenkins+Trac+StatSVN 让你迅速了解整个开发的方方面面（无死角）

### hooray boot开发平台在未来会拥有以下特性
- 基于Spring Boot/MyBatise/JPA/JdbcTemplate，快速构建基于SpringMVC为核心的MVC应用程序，同时方便提供Restful的方式发布 
- 可打包成可执行的JAR方式与基于Tomcat容器的WAR方式
- 提供良好的分层机制，多应用之间可通过JMS(hornet)/RPC/Restful方式交互数据
- 可对每个符合Spring Boot的应用程序进行可视化运维
- 基于Flyway的数据库Migrate管理，便于数据库版本迁移与升级
- 前端使用bower管理JS/CSS
- hazelcast支持，可用在spring-boot-admin中做服务发现，也可用在分布式集合java.util.{Map,List,Set}锁等
- 基于redis做分布式session存储
- SSO单点登录，提供一个SSO模块，方便在架构开始，就避免日后多项目产品出现各自用户体系管理难的问题，一点登录，全网傲游，你值得拥有
- Quartz集成，保存到MySQL中（http://www.tuicool.com/articles/b6NBzi）

### 使用到第三方库及平台

- https://github.com/spring-projects/spring-boot
- https://github.com/codecentric/spring-boot-admin
- https://github.com/hazelcast/hazelcast
- https://github.com/flyway/flyway
- https://github.com/antirez/redis 
- nodejs bower

### nodejs bower工具的简单使用

1. 安装nodejs
	linux： sudo npm install -g bower
2. 进入到项目，创建一个bower.json文件，并按照bower的格式填充js/css库
	
		{
		  "name": "hooray-panel",
		  "dependencies": {
		    "angular": "~1.3.9",
		    "angular-resource": "~1.3.9",
		    "bootstrap-css-only": "~3.3.5"
		  }
		}

3. 在与bower.json同层文件夹中执行：
 
 	bower install 

4. 如何跑起？

4.1 启动redis

4.2 分别启动hooray-panel，hooray-mpweb（PS：该项目没啥内容，就纯测试）

4.3 启动hooray-sadmin，所有基于springboot的应用管理中心，看下面截图

4.4 如果是首次运行项目，需要在`hooray-panel`中执行`mvn flyway:init -Dmaven.test.skip=true` , `mvn flyway:migrate -Dmaven.test.skip=true`,具体可
	查看`hooray-panel/pom.xml`。
	另外，电商的sql，未归到flyway管理，而是在`hooray-mp/hooray-shop/src/main/resources/hooray_shop.sql`中，直接导入数据库即可
4.5 其他注意问题，当你利用mybatise自动生成代码工具生成各个表的对应的
5. 研究Pivot or Javafx 做Web Chart数据展示

### hooray-sadmin的运行效果图展示

应用程序列表首页

![hooray-application-index](./doc/images/index.jpg)

Journal日志截图
![hooray-application-journal](./doc/images/journal.jpg)