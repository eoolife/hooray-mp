### Spring Boot Debug Running

配置： mvn spring-boot:run --debug


### @AutoWired 对应的服务类有多种实现的方式


正确情况下：

	某个接口的实现类配置
	
	@Service("accountService")
	public class MpAccountServiceImpl implements MpAccountService
	
	@Service("accountService2")
	public class MpAccountServiceImpl2 implements MpAccountService
	
	// ==============================================
	
	// 第一种情况可以正确运行
	
	@Autowired
	private MpAccountService accountService;
	
	@Autowired
	private MpAccountService accountService2;

	// 以下的情况 accountService22注入失败，对比实现类配置的名称与该处的名称
	@Autowired
	private MpAccountService accountService;
	
	@Autowired
	private MpAccountService accountService22;