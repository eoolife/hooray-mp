package in.hooray.ws.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.wss4j.common.ext.WSPasswordCallback;

public class ClientPasswordCallback implements CallbackHandler {

	 /*
     * public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
     * WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
     * if ("max".equals(pc.getIdentifier())) {
     * pc.setPassword("maxPassword");
     * } // else {...} - can add more users, access DB, etc.
     * }
     */
    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (Callback callback : callbacks) {
            if (callback instanceof WSPasswordCallback)
                handle((WSPasswordCallback) callback);
        }

    }

    private void handle(WSPasswordCallback callback) {
        callback.setPassword("123456");
    }

}
