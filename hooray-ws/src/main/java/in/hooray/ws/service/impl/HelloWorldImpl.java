package in.hooray.ws.service.impl;

import in.hooray.ws.service.HelloWorldWS;

import javax.jws.WebService;

import org.springframework.stereotype.Service;

@WebService(endpointInterface = "in.hooray.ws.service.HelloWorldWS")
@Service("helloWorld")
public class HelloWorldImpl implements HelloWorldWS {

	@Override
	public String sayHi(String text) {
        return "Hello " + text;
	}

}
