package in.hooray.mpweb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@Configuration
@ImportResource(locations={"mybatise-spring.xml"})
@ComponentScan(basePackages={"in.hooray.mpweb", "in.hooray.entity", "in.hooray.service", "in.hooray.mpsdk.plugin.impl"})
public class MpwebConfig {

}
