package in.hooray.mpweb.controller;

import java.util.List;
import java.util.Optional;

import in.hooray.mpsdk.plugin.IPlugin;
import in.hooray.mpsdk.plugin.PluginParam;
import in.hooray.mpweb.WxPlugins;
import in.hooray.mpweb.config.MpsdkProperties;
import in.hooray.service.mp.MpAccountService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.sd4324530.fastweixin.message.BaseMsg;
import com.github.sd4324530.fastweixin.message.TextMsg;
import com.github.sd4324530.fastweixin.message.req.BaseEvent;
import com.github.sd4324530.fastweixin.message.req.LocationEvent;
import com.github.sd4324530.fastweixin.message.req.MenuEvent;
import com.github.sd4324530.fastweixin.message.req.QrCodeEvent;
import com.github.sd4324530.fastweixin.message.req.ScanCodeEvent;
import com.github.sd4324530.fastweixin.message.req.SendPicsInfoEvent;
import com.github.sd4324530.fastweixin.message.req.TemplateMsgEvent;
import com.github.sd4324530.fastweixin.message.req.TextReqMsg;
import com.github.sd4324530.fastweixin.servlet.WeixinControllerSupport;


@RestController
@RequestMapping("/wx")
public class WeixinApiController extends WeixinControllerSupport {
	
	@Autowired
	private MpsdkProperties mpProp;
	
	@Autowired
	private MpAccountService accountService;
	
	@Autowired
	private WxPlugins wxPlugins;
	
	private static final Logger log = LoggerFactory.getLogger(WeixinApiController.class);
    
	public void index(){
		System.out.println("=====================================");
		System.out.println(accountService.findAccount("ACCOUNT_NAME"));
	}

	@Override
	protected String getToken() {
		return mpProp.getToken();
	}
	
	//使用安全模式时设置：APPID
    //不再强制重写，有加密需要时自行重写该方法
    @Override
    protected String getAppId() {
        return mpProp.getAppid();
    }
    //使用安全模式时设置：密钥
    //不再强制重写，有加密需要时自行重写该方法
    @Override
    protected String getAESKey() {
        return mpProp.getAesKey();
    }
    //重写父类方法，处理对应的微信消息
    @Override
    protected BaseMsg handleTextMsg(TextReqMsg msg) {
        String content = msg.getContent();
        log.debug("用户发送到服务器的内容:{}", content);
        
        Optional<List<IPlugin>> pluginsOptional = Optional.empty();
        List<IPlugin> plugins = pluginsOptional.orElse(wxPlugins.getPlugins());
        for(IPlugin plugin : plugins) {
        	PluginParam param = new PluginParam();
			plugin.execute(param);
        }
        
        return new TextMsg("感谢您关注我们！");
    }
    
    @Override
    protected BaseMsg handleSubscribe(BaseEvent event) {
    	
    	return super.handleSubscribe(event);
    }
    
    @Override
    protected BaseMsg handleUnsubscribe(BaseEvent event) {
    	
    	return super.handleUnsubscribe(event);
    }
    
    @Override
    protected BaseMsg handleLocationEvent(LocationEvent event) {
    	
    	return super.handleLocationEvent(event);
    }
    
    @Override
    protected BaseMsg handleMenuClickEvent(MenuEvent event) {
    	
    	return super.handleMenuClickEvent(event);
    }
    
    @Override
    protected BaseMsg handleMenuViewEvent(MenuEvent event) {
    	
    	return super.handleMenuViewEvent(event);
    }
    
    @Override
    protected BaseMsg handleQrCodeEvent(QrCodeEvent event) {
    	
    	return super.handleQrCodeEvent(event);
    }
    
    @Override
    protected BaseMsg handleScanCodeEvent(ScanCodeEvent event) {
    	
    	return super.handleScanCodeEvent(event);
    }
    
    @Override
    protected BaseMsg handleTemplateMsgEvent(TemplateMsgEvent event) {
    	
    	return super.handleTemplateMsgEvent(event);
    }
    
    @Override
    protected BaseMsg handlePSendPicsInfoEvent(SendPicsInfoEvent event) {
    	
    	return super.handlePSendPicsInfoEvent(event);
    }
    
    
}
