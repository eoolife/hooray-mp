package in.hooray.mpweb;

import in.hooray.core.quartz.SchedulerConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({SchedulerConfig.class})
public class MpwebRunner {
    public static void main( String[] args ) {
    	SpringApplication.run(MpwebRunner.class, args);
    }
}
