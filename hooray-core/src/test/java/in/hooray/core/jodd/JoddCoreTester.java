package in.hooray.core.jodd;

import jodd.mail.Email;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JoddCoreTester {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEmail() {
		Email email = Email.create()
				.from("daqing15@163.com")
				.to("eoolife@163.com")
				.subject("呼吁您哟哟")
				.addText("第三方斯蒂芬斯蒂芬斯蒂芬森的");
		SmtpServer smtpServer = SmtpServer.create("smtp.163.com")
				.authenticateWith("daqing15@163.com", "Daqing1221");
		SendMailSession session = smtpServer.createSession();
		session.open();
		session.sendMail(email);
		session.close();
	}

}
