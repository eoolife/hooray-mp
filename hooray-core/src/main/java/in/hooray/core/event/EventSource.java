package in.hooray.core.event;


import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;

public class EventSource {
	
	private EventType eventType;
	
	private Map<String, Object> params = Maps.newHashMap();
	
	public EventSource(){}
	
	public EventSource(EventType eventType){
		this.eventType = eventType;
	}
	
	public EventSource set(String key, Object value){
		if(StringUtils.isNotEmpty(key) && value != null){
			params.put(key, value);
		}
		return this;
	}
	
	public Object get(String key) {
		if(StringUtils.isNotEmpty(key) && params.containsKey(key)){
			return params.get(key);			
		} else {
			return null;
		}
	}
	
	public Map<String, Object> getParams() {
		return params;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}
	
}
