package in.hooray.core.common;

import jodd.util.PropertiesUtil;

public class Constants {

	// 系统根目录
	public static final String ROOT_PATH = System.getProperty("user.dir");
	
	public static final String config="mongo.properties";
	
	public static final String charset="utf-8";
	
	public static int SERVER_PORT ;
	
	public static String SERVER_HOST; 
	
	public static String SERVER_HOSTS; 
	
	static {
		try {
			SERVER_PORT = Integer.parseInt(PropertiesUtil.createFromFile(config).getProperty("mongodb.port"));
			SERVER_HOST = PropertiesUtil.createFromFile(config).getProperty("mongodb.ip");
			SERVER_HOSTS = PropertiesUtil.createFromFile(config).getProperty("mongodb.ips");
		}catch(Exception ex){
			SERVER_PORT = 27017;
			SERVER_HOST = "127.0.0.1";
			SERVER_HOST = "";
			System.out.println("加载mongod配置文件失败");
			ex.printStackTrace();
		}
	}
	
	
}
