package in.hooray.rpc.demo;

import com.baidu.jprotobuf.pbrpc.transport.RpcServer;

public class RpcServerPush {
	
	public static void main(String[] args) {
		RpcServer rpcServer = new RpcServer();
	    EchoServiceImpl echoServiceImpl = new EchoServiceImpl();
	    rpcServer.registerService(echoServiceImpl);
	    rpcServer.start(1031);
	}

}
