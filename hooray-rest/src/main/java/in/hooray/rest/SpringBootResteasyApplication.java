package in.hooray.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class SpringBootResteasyApplication 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(SpringBootResteasyApplication.class, args);
    }
}
