package in.hooray.panel.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "qiniu", ignoreUnknownFields = false)
@Component
public class QiniuSettings {
	
	private String ak;
	private String sk;
	private String bucket;
	private String fileHost;
	
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	public String getSk() {
		return sk;
	}
	public void setSk(String sk) {
		this.sk = sk;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getFileHost() {
		return fileHost;
	}
	public void setFileHost(String fileHost) {
		this.fileHost = fileHost;
	}
}
