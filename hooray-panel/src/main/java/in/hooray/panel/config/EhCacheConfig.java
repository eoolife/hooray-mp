//package in.hooray.panel.config;
//
//import org.springframework.cache.CacheManager;
//import org.springframework.cache.annotation.CachingConfigurer;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cache.ehcache.EhCacheCacheManager;
//import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
//import org.springframework.cache.interceptor.CacheErrorHandler;
//import org.springframework.cache.interceptor.CacheResolver;
//import org.springframework.cache.interceptor.KeyGenerator;
//import org.springframework.cache.interceptor.SimpleCacheErrorHandler;
//import org.springframework.cache.interceptor.SimpleCacheResolver;
//import org.springframework.cache.interceptor.SimpleKeyGenerator;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//
///**
// * ehcache缓存配置
//*/
//@Configuration
//@EnableCaching
//public class EhCacheConfig implements CachingConfigurer {
//
//	@Bean
//	public EhCacheManagerFactoryBean ehCacheManagerFactoryBean() {
//		EhCacheManagerFactoryBean ehCacheManagerFactoryBean = new EhCacheManagerFactoryBean();
//		ehCacheManagerFactoryBean.setConfigLocation(new ClassPathResource("ehcache.xml"));
//		ehCacheManagerFactoryBean.setCacheManagerName("ehCacheManager");
//		ehCacheManagerFactoryBean.setShared(true);
//		return ehCacheManagerFactoryBean;
//	}
//
//	@Bean
//	@Override
//	public CacheManager cacheManager() {
//		EhCacheCacheManager cacheManager = new EhCacheCacheManager();
//		cacheManager.setCacheManager(ehCacheManagerFactoryBean().getObject());
//		return cacheManager;
//	}
//
//	@Override
//	public CacheResolver cacheResolver() {
//		return new SimpleCacheResolver(cacheManager());
//	}
//
//	@Override
//	public CacheErrorHandler errorHandler() {
//		return new SimpleCacheErrorHandler();
//	}
//
//	@Override
//	public KeyGenerator keyGenerator() {
//		return new SimpleKeyGenerator();
//	}
//
//}
