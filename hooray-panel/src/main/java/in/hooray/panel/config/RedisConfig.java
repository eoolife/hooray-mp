package in.hooray.panel.config;
//package in.hjtapi.app.config;
//
//import java.lang.reflect.Method;
//
//import org.springframework.cache.CacheManager;
//import org.springframework.cache.annotation.CachingConfigurerSupport;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cache.interceptor.KeyGenerator;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.cache.RedisCacheManager;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
//
//import com.fasterxml.jackson.annotation.JsonAutoDetect;
//import com.fasterxml.jackson.annotation.PropertyAccessor;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//@Configuration
//@EnableCaching
//public class RedisConfig extends CachingConfigurerSupport{
//	
//	@Bean
//	public KeyGenerator heiyoKeyGenerator() {
//		return new KeyGenerator() {
//			@Override
//			public Object generate(Object target, Method method, Object... params) {
//				StringBuilder sb = new StringBuilder();  
//                sb.append(target.getClass().getName());  
//                sb.append(method.getName());  
//                for (Object obj : params) {  
//                    sb.append(obj.toString());  
//                }  
//                return sb.toString(); 
//			}
//		};
//	}
//	
//	@Bean  
//    public CacheManager cacheManager(  
//            @SuppressWarnings("rawtypes") RedisTemplate redisTemplate) {  
//        return new RedisCacheManager(redisTemplate);  
//    }
//	
//	/**
//	 * jackson的自动检测机制
//	 * 	<pre>
//	 * 	@JsonAutoDetect（作用在类上）来开启/禁止自动检测
//			fieldVisibility:字段的可见级别
//			ANY:任何级别的字段都可以自动识别
//			NONE:所有字段都不可以自动识别
//			NON_PRIVATE:非private修饰的字段可以自动识别
//			PROTECTED_AND_PUBLIC:被protected和public修饰的字段可以被自动识别
//			PUBLIC_ONLY:只有被public修饰的字段才可以被自动识别
//			DEFAULT:同PUBLIC_ONLY
//			jackson默认的字段属性发现规则如下：
//			所有被public修饰的字段->所有被public修饰的getter->所有被public修饰的setter
//		</pre>
//	 * @param factory
//	 * @return
//	 */
//	@Bean  
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//    public RedisTemplate<String, String> redisTemplate(  
//            RedisConnectionFactory factory) {  
//        StringRedisTemplate template = new StringRedisTemplate(factory);  
//		Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);  
//        ObjectMapper om = new ObjectMapper();  
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);  
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);  
//        jackson2JsonRedisSerializer.setObjectMapper(om);  
//        template.setValueSerializer(jackson2JsonRedisSerializer);  
//        template.afterPropertiesSet();  
//        return template;  
//    } 
//}
