//package in.hooray.panel.config.mybatise;
//
//import in.hooray.core.mybatis.plugin.MyMapper;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.bind.RelaxedPropertyResolver;
//import org.springframework.context.EnvironmentAware;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.core.io.DefaultResourceLoader;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import tk.mybatis.spring.mapper.MapperScannerConfigurer;
//
//@Configuration
//@ConditionalOnClass({ EnableTransactionManagement.class})  
//@MapperScan(basePackages="in.hooray.entity.**.mapper")
//@ComponentScan(basePackages={"in.hooray.service", "in.hooray.entity"})  
//public class MybatisConfiguration implements EnvironmentAware {
//
//	private static Log logger = LogFactory.getLog(MybatisConfiguration.class);
//
//	private RelaxedPropertyResolver propertyResolver;
//
//	@Autowired
//	private DataBaseConfig dataBaseConfig;
//
//	@Override
//	public void setEnvironment(Environment environment) {
//		this.propertyResolver = new RelaxedPropertyResolver(environment, "mybatis.");
//	}
//	
//	/**
//	 *  <bean class="tk.mybatis.spring.mapper.MapperScannerConfigurer">
//	        <property name="basePackage" value="com.isea533.mybatis.mapper"/>
//	        <!-- 3.2.2版本新特性，markerInterface可以起到mappers配置的作用，详细情况需要看Marker接口类 -->
//	        <property name="markerInterface" value="com.isea533.mybatis.util.MyMapper"/>
//	        -->
//	    </bean>
//	 * @return
//	 */
//	@Bean
//	public MapperScannerConfigurer mapperScannerConfigurer() {
//		MapperScannerConfigurer configurer = new MapperScannerConfigurer();
//		configurer.setBasePackage("in.hooray.entity");
//		configurer.setMarkerInterface(MyMapper.class);
//		return configurer;
//	}
//	
//	/*
//	@Bean
//	public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryBean") SqlSessionFactory sqlSessionFactory) {
//		SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
//		return sqlSessionTemplate;
//	}*/
//	
//	@Bean //(name="sqlSessionFactory")
//	public SqlSessionFactory sqlSessionFactory() {
//		try {
//			SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
//			sessionFactory.setDataSource(dataBaseConfig.dataSource());
//			sessionFactory.setTypeAliasesPackage(propertyResolver.getProperty("typeAliasesPackage"));
//			sessionFactory.setConfigLocation(new DefaultResourceLoader()
//				.getResource(propertyResolver.getProperty("configLocation")));
//			// 注入mybatis mapper xml文件
//			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//			sessionFactory.setMapperLocations(resolver.getResources("classpath:sqlMapperXml/**/*.xml"));
//			return sessionFactory.getObject();
//		} catch (Exception e) {
//			logger.warn("Could not confiure mybatis session factory");
//			return null;
//		}
//	}
//
//	@Bean(name="transactionManager")
//	public DataSourceTransactionManager transactionManager() { 
//		return new DataSourceTransactionManager(dataBaseConfig.dataSource());
//	}
//}
