package in.hooray.panel.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface IFileHandler {

	Map<String, Object> getToken(String appId, String name);
	
	Map<String, Object> callback(String body, HttpServletRequest request) throws Exception;
}
