package in.hooray.panel.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * 剩下的就是corn表达式了、具体使用以及参数请百度google、
	下面只例出几个式子
	CRON表达式    含义 
	"0 0 12 * * ?"    每天中午十二点触发 
	"0 15 10 ? * *"    每天早上10：15触发 
	"0 15 10 * * ?"    每天早上10：15触发 
	"0 15 10 * * ? *"    每天早上10：15触发 
	"0 15 10 * * ? 2005"    2005年的每天早上10：15触发 
	"0 * 14 * * ?"    每天从下午2点开始到2点59分每分钟一次触发 
	"0 0/5 14 * * ?"    每天从下午2点开始到2：55分结束每5分钟一次触发 
	"0 0/5 14,18 * * ?"    每天的下午2点至2：55和6点至6点55分两个时间段内每5分钟一次触发 
	"0 0-5 14 * * ?"    每天14:00至14:05每分钟一次触发 
	"0 10,44 14 ? 3 WED"    三月的每周三的14：10和14：44触发 
	"0 15 10 ? * MON-FRI"    每个周一、周二、周三、周四、周五的10：15触发 
 * @author daqing
 *
 */
@Component
public class DemoJob {
	
//	public void methodA() {
//		while (i++ < 10) {
//			System.out.println("==============Time:" + i);
//		}
//	}
//	
//	/** 
//     * cron表达式：* * * * * *（共6位，使用空格隔开，具体如下） 
//     * cron表达式：*(秒0-59) *(分钟0-59) *(小时0-23) *(日期1-31) *(月份1-12或是JAN-DEC) *(星期1-7或是SUN-SAT) 
//     */  
//  
//    /** 
//     * 定时卡点计算。每天凌晨 02:00 执行一次 
//     */  
//    @Scheduled(cron = "0 0 2 * * *")  
//    public void autoCardCalculate() {  
//        System.out.println("定时卡点计算... " + new Date());  
//    }  
//  
//    /** 
//     * 心跳更新。启动时执行一次，之后每隔1分钟执行一次 
//     */  
//    @Scheduled(fixedRate = 1000*60*1)  
//    public void heartbeat() {  
//        System.out.println("心跳更新... " + new Date());  
//    }  
//  
//    /** 
//     * 卡点持久化。启动时执行一次，之后每隔2分钟执行一次 
//     */  
//    @Scheduled(fixedRate = 1000*60*2)  
//    public void persistRecord() {  
//        System.out.println("卡点持久化... " + new Date());  
//    } 
	
	@Scheduled(fixedDelay=5000 * 60)
	public void doSomething() {
		System.out.println("注解方式执行。。。");
	}
}
