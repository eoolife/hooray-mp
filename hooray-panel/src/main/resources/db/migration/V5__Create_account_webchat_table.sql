CREATE TABLE `tb_account_wechats` (
	`acid` INT(10) UNSIGNED NOT NULL,
	`uniacid` INT(10) UNSIGNED NOT NULL,
	`token` VARCHAR(32) NOT NULL,
	`level` TINYINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`name` VARCHAR(30) NOT NULL,
	`wx_account` VARCHAR(30) NOT NULL COMMENT '微信公众号的微信帐号',
	`industry_ids` VARCHAR(30) NOT NULL COMMENT '行业代码',
	`original` VARCHAR(50) NULL DEFAULT NULL COMMENT '公众号原始ID',
	`signature` VARCHAR(500) NULL DEFAULT NULL COMMENT '首次关注回复内容',
	`address` VARCHAR(150) NULL DEFAULT NULL COMMENT '公众号联系地址',
	`linkman` VARCHAR(30) NOT NULL COMMENT '该公众号的联系人',
	`linkphone` VARCHAR(16) NOT NULL COMMENT '该公众号的联系人电话',
	`appid` VARCHAR(50) NULL DEFAULT NULL,
	`secret` VARCHAR(50) NULL DEFAULT NULL,
	`aeskey` VARCHAR(50) NULL DEFAULT NULL,
	`qrcodeurl` VARCHAR(255) NULL DEFAULT NULL,
	`addtime` DATETIME NOT NULL,
	`styleid` INT(10) UNSIGNED NULL DEFAULT '1',
	PRIMARY KEY (`acid`),
	INDEX `idx_key` (`appid`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB