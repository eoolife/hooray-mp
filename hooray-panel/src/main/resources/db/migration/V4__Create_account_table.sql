CREATE TABLE `tb_account` (
	`acid` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uniacid` INT(10) UNSIGNED NOT NULL,
	`hash` CHAR(8) NOT NULL,
	`type` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	`isconnect` TINYINT(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`acid`),
	UNIQUE INDEX `hash` (`hash`),
	INDEX `idx_uniacid` (`uniacid`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=24