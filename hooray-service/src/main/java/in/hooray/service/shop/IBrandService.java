package in.hooray.service.shop;

import in.hooray.entity.shop.ShopBrand;
import in.hooray.entity.shop.ShopBrandCategory;

public interface IBrandService {

	public ShopBrandCategory getBrandCategoryById(Integer categoryId);
	
	public ShopBrand getBrandById(Integer brandId);
	
	public Integer saveBrand(ShopBrand brand);
	
	public Integer saveBrandCategory(ShopBrandCategory brandCategory);
	
}
