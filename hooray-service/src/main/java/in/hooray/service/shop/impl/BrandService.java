package in.hooray.service.shop.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.hooray.entity.mapper.shop.ShopBrandCategoryMapper;
import in.hooray.entity.mapper.shop.ShopBrandMapper;
import in.hooray.entity.shop.ShopBrand;
import in.hooray.entity.shop.ShopBrandCategory;
import in.hooray.service.shop.IBrandService;


@Service("brandService")
public class BrandService implements IBrandService {
	
	@Autowired
	ShopBrandMapper brandMapper;
	
	@Autowired
	ShopBrandCategoryMapper brandCategoryMapper; 

	@Override
	public ShopBrandCategory getBrandCategoryById(Integer categoryId) {
		return brandCategoryMapper.selectByPrimaryKey(categoryId);
	}

	@Override
	public ShopBrand getBrandById(Integer brandId) {
		
		return brandMapper.selectByPrimaryKey(brandId);
	}

	@Override
	public Integer saveBrand(ShopBrand brand) {
		
		return brandMapper.insert(brand);
	}

	@Override
	public Integer saveBrandCategory(ShopBrandCategory brandCategory) {
		
		return brandCategoryMapper.insert(brandCategory);
	}

}
