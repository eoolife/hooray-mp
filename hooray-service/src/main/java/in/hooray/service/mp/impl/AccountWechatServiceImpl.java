package in.hooray.service.mp.impl;

import in.hooray.entity.mp.AccountWechat;
import in.hooray.entity.mp.AccountWechatCriteria;
import in.hooray.service.BaseService;
import in.hooray.service.mp.AccountWechatService;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tk.mybatis.mapper.entity.Example;

import com.github.pagehelper.PageHelper;


@Service("accountWechatService")
@Transactional
public class AccountWechatServiceImpl extends BaseService<AccountWechat> implements AccountWechatService {
	

	@Override
	public List<AccountWechat> findAccuntWechats(AccountWechatCriteria criteria) {
		return null;
	}

	@Override
	public AccountWechat getAccuntWechat(AccountWechatCriteria criteria) {
		return null;
	}

	@Override
	public AccountWechat getAccuntWechatByPk(Integer accountId) {
		return selectByKey(accountId); 
	}

	@Override
	public AccountWechat selectByKey(Object key) {
		return null;
	}

	@Override
	public int save(AccountWechat entity) {
		return 0;
	}

	@Override
	public int delete(Object key) {
		return 0;
	}

	@Override
	public int updateAll(AccountWechat entity) {
		return 0;
	}

	@Override
	public int updateNotNull(AccountWechat entity) {
		return 0;
	}

	@Override
	public List<AccountWechat> selectByExample(Object example) {
		return this.mapper.selectByExample(example);
	}

	@Override
	public List<AccountWechat> selectByAccountWechat(AccountWechat wechat, int page, int rows) {
		Example example = new Example(AccountWechat.class);
		Example.Criteria criteria = example.createCriteria();
		if(StringUtils.isNotEmpty(wechat.getName())) {
			criteria.andLike("name", "%" + wechat.getName() + "%");
		}
		if(wechat.getAcid() != null) {
			criteria.andEqualTo("acid", wechat.getAcid());
		}
		PageHelper.startPage(page, rows);
		return selectByExample(example);
	}

}
