package in.hooray.service.mp.impl;

import in.hooray.entity.mp.MpAccount;
import in.hooray.service.mp.MpAccountService;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;


@Service("accountService2")
public class MpAccountServiceImpl2 implements MpAccountService {

	@Override
	public List<MpAccount> findAccount(String accountName) {
		List<MpAccount> accounts = Lists.newArrayList();
		accounts.add(new MpAccount(1L, "accountService2", "b", accountName));
		accounts.add(new MpAccount(2L, "accountService22", "bb", accountName));
		return accounts;
	}

}
