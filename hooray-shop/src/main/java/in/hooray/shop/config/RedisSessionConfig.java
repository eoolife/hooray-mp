package in.hooray.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * EnableRedisHttpSession 
 * 	annotation creates a Spring Bean with the name of springSessionRepositoryFilter that implements Filter.
 * 	The filter is what is in charge of replacing the HttpSession implementation to be backed by Spring Session. 
 *  In this instance Spring Session is backed by Redis.
 * @author daqing
 *
 */
@Configuration
@EnableRedisHttpSession
public class RedisSessionConfig {
	
	@Bean
    public JedisConnectionFactory connectionFactory() {
            return new JedisConnectionFactory();  
    }

}
