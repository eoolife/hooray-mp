package in.hooray.shop.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 商品分类管理
 * @author chendaqing
 *
 */
@Controller
@RequestMapping(value = "/admin/goodsCategory")
public class GoodsCategoryManagerController {
	
	public String index() {
		return "";
	}
	
}
