package in.hooray.shop.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import in.hooray.entity.shop.ShopBrandCategory;
import in.hooray.service.shop.IBrandService;
import in.hooray.shop.controller.BaseController;

/**
 * 商品品牌管理
 * @author darking
 *
 */
@Controller
@RequestMapping(value = "/admin/brand")
public class BrandManagerController extends BaseController {
	
	@Autowired
	IBrandService brandService;

	@RequestMapping(value = "/edit/{brandId}", method = RequestMethod.GET)
	public String doEdit(@PathVariable("brandId") Integer brandId, Integer categoryId) {
		
		//编辑品牌分类 读取品牌分类信息
		if(categoryId == null) {
			return REDIRECT + "/admin/brand";
		}
		ShopBrandCategory brandCategory = brandService.getBrandCategoryById(categoryId);
		
		return "";
	}
	
	@RequestMapping(value = "/")
	public String index() {
		
		return "";
	}
	
}
